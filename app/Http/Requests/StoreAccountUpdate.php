<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreAccountUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->user()->hasRole(['docent'])){
            return true;
        }elseif($this->user()->hasRole(['leerling']) && ($this->user()->id == $this->user->id)){
            return true;
        }else{
            return abort(403);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->user()->hasRole(['docent'])){
            return [
                'id'          => 'max:9999999',Rule::unique('users')->ignore($this->user->id),
                'firstname'   => 'required|max:255',
                'prefix'      => 'max:255',
                'lastname'    => 'required|max:255',
                'email'       => 'required|email',Rule::unique('users')->ignore($this->user->id),
                'dateofbirth' => 'date',
                'address'     => 'max:255',
                'zipcode'     => 'max:255',
                'city'        => 'max:255',
                'phonenumber' => 'max:255',
                'allergies'   => 'max:255',
                'size'        => 'max:10',
                'calamity'    => 'max:255',
                'hygiene'     => 'required|boolean',
                'bhv'         => 'required|boolean',
                'notes'       => 'max:2000',
            ];
        }else{
            return [
                'allergies'   => 'max:255',
                'size'        => 'max:10',
                'calamity'    => 'max:255',
                'hygiene'     => 'required|boolean',
                'bhv'         => 'required|boolean',
            ];
        }

    }
}