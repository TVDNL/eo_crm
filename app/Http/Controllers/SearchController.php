<?php

namespace App\Http\Controllers;

use App\Event;
use App\Group;
use App\Task;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function find(Request $request)
    {
        // c = category, q = search query
        switch($request->get('c')){
            case 'users':
                return self::searchUsers($request->get('q'));
                break;
            case 'events':
                return self::searchEvents($request->get('q'));
                break;
            case 'groups':
                return self::searchGroups($request->get('q'));
                break;
            case 'tasks':
                return self::searchTasks($request->get('q'));
                break;
            default:
                return '';
                break;
        }
    }

    public function searchUsers($query)
    {
        if(Auth::user()->hasRole(['opdrachtgever'])){
            //Clients may only get search results of the students that are accepted to the clients event(s).
            $events = Auth::user()->events()->get();
            $users = [];
            foreach ($events as $event){
                $users = array_unique(
                    array_merge(
                        $users,
                        $event->participants()
                            ->with(
                                [
                                    'roles' => function($q){
                                        $q->where('name', '=', 'leerling');
                                    }
                                ]
                            )
                            ->whereHas('roles', function($q){
                                $q->where('name', '=', 'leerling');
                            })
                            ->where('event_user.status', '=', 'accepted')
                            ->search($query)
                            ->get()
                            ->toArray()
                    ),
                    SORT_REGULAR
                );
            }
            return $users;
        }elseif(Auth::user()->hasRole(['docent'])){
            //Teachers are able to see everyone's profile
            return User::search($query)->with('roles')->get();
        }elseif(Auth::user()->hasRole(['leerling'])){
            //Students can only see their own profile so no need for search results
            return '';
        }else{
            return '';
        }
    }

    public function searchEvents($query)
    {
        if(Auth::user()->hasRole(['opdrachtgever'])){
            return Auth::user()->events()->search($query)->get();
        }elseif(Auth::user()->hasRole(['docent'])){
            return Event::search($query)->get();
        }elseif(Auth::user()->hasRole(['leerling'])){
            return Event::search($query)->get();
        }else{
            return '';
        }
    }

    public function searchGroups($query)
    {
        if(Auth::user()->hasRole(['opdrachtgever'])){
            return Group::search($query)->get();
        }elseif(Auth::user()->hasRole(['docent'])){
            return Group::search($query)->get();
        }elseif(Auth::user()->hasRole(['leerling'])){
            return Group::search($query)->get();
        }else{
            return '';
        }
    }

    public function searchTasks($query)
    {
        if(Auth::user()->hasRole(['opdrachtgever'])){
            $events = Auth::user()->events()->get();
            $tasks = [];
            foreach($events as $event){
                $tasks = array_merge($tasks, $event->tasks()->search($query)->with('event')->get()->toArray());
            }
            return $tasks;
        }elseif(Auth::user()->hasRole(['docent'])){
            return Task::search($query)->with('event')->get();
        }elseif(Auth::user()->hasRole(['leerling'])){
            return Task::whereHas('assignees', function($q) {
                            $q->where('id', '=', Auth::id());
                        })
                        ->search($query)->with('event')
                        ->get();
        }else{
            return '';
        }
    }
}