<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->hasRole(['docent', 'opdrachtgever'])) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required',
            'description' => 'max:2000',
            'maxstudents' => '',
            'credit'      => 'required|time_credit',
            'startdate'   => 'required',//currently the form returns a custom format
            'enddate'     => 'required',//
            'level'       => 'array|in:1,2,3'
        ];
    }
}
