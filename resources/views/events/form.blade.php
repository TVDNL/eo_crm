@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card card-block sameheight-item">
                    <div class="title-block">
                        <h3 class="title">
                            {{$title}}
                        </h3>
                    </div>
                    <hr>
                    @include('errors.errors')
                    <form action="{{$route}}" method="POST">
                        {{ csrf_field() }}
                        @if(Laratrust::hasRole(['docent']))
                            <div class="form-group row">
                                <label class="col-xs-3 col-form-label"><span class="fa fa-info-circle input_help" title="{{trans('input.event_type')}}"></span> Type:*</label>
                                <div class="col-xs-9">
                                    <label class="custom-control custom-radio">
                                        <input id="radio1" name="type" type="radio" class="custom-control-input" value="public" @if(old('type') == null) @if($event->type == 'Public') checked @endif @elseif(old('type') == 'public') checked @endif required>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{trans('messages.public')}}</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input id="radio2" name="type" type="radio" class="custom-control-input" value="personal" @if(old('type') == null) @if($event->type == 'Personal') checked @endif @elseif(old('type') == 'personal') checked @endif required>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{trans('messages.personal')}}</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xs-3 col-form-label">Status:*</label>
                                <div class="col-xs-9">
                                    <label class="custom-control custom-radio">
                                        <input id="radio3" name="status" type="radio" class="custom-control-input" value="1" @if(old('status') == null) @if($event->status == trans('messages.active')) checked @endif @elseif(old('status') == '1') checked @endif required>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{trans('messages.active')}}</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input id="radio4" name="status" type="radio" class="custom-control-input" value="0" @if(old('status') == null) @if($event->status == trans('messages.hidden')) checked @endif @elseif(old('status') == '0') checked @endif required>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">{{trans('messages.hidden')}}</span>
                                    </label>
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label for="title" class="col-sm-3 form-control-label">{{trans('messages.title')}}:*</label>
                            <div class="col-sm-9">
                                <input name="title" type="text" class="form-control" id="title" maxlength="255" value="@if(old('title') == null){{ $event->title }}@else{{old('title')}}@endif">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 form-control-label">{{trans('messages.description')}}:</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control" id="description" maxlength="2000" rows="7" style="resize: none">@if(old('description') == null){{ $event->description }}@else{{old('description')}}@endif</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dates" class="col-sm-3 form-control-label">Start & {{trans('messages.end')}}:</label>
                            <div class='col-sm-4'>
                                <div class="form-group">
                                    <div class='input-group date' id='startdate'>
                                        <input type='text' class="form-control" name="startdate" value="{{date_format(date_create($event->startDate), 'd/m/Y  H:i')}}" required>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class='col-sm-4'>
                                <div class="form-group">
                                    <div class='input-group date' id='enddate'>
                                        <input type='text' class="form-control" name="enddate" value="{{date_format(date_create($event->endDate), 'd/m/Y  H:i')}}" required/>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                        <div class="form-group row">
                            <label for="total_hours" class="col-sm-3 form-control-label">{{trans('messages.total_hours')}}:</label>
                            <div class="col-sm-2">
                                <input name="total_hours" type="text" class="form-control" id="total_hours" value="@if(old('total_hours') == null){{ $event->total_hours }}@else{{old('total_hours')}}@endif">
                            </div>
                            <div class="col-sm-7"></div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-3 form-control-label">{{trans('messages.edu_level')}}:</label>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name='level[]' value="1" min="1" max="3" @if(in_array('1', explode(', ', $event->educational_level))) checked @endif > 1
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name='level[]' value="2" min="1" max="3" @if(in_array('2', explode(', ', $event->educational_level))) checked @endif > 2
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name='level[]' value="3" min="1" max="3" @if(in_array('3', explode(', ', $event->educational_level))) checked @endif > 3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-3 form-control-label">{{trans('messages.number_students')}}:</label>
                            <div class="col-sm-2">
                                <input name='min_students' type="number" min="1" class="form-control" id="min_students" placeholder="Min" value="@if(old('min_students') == null){{ $event->min_users }}@else{{old('min_students')}}@endif">
                            </div>
                            <div class="col-sm-2">
                                <input name='max_students' type="number" class="form-control" id="max_students" placeholder="Max" value="@if(old('max_students') == null){{ $event->max_users }}@else{{old('max_students')}}@endif">
                            </div>
                            <div class="col-sm-5"></div>
                        </div>
                        @if(Laratrust::hasRole(['admin', 'docent']))
                            <div class="form-group row">
                                <label for="opdrachtgevers" class="col-sm-3 form-control-label">{{trans('messages.client_s')}}:</label>
                                <div class="col-sm-9">
                                    <select multiple name="opdrachtgevers[]" id="opdrachtgevers">
                                        @foreach($opdrachtgevers as $opdrachtgever)
                                            <option value="{{$opdrachtgever->id}}" @if($event->participants->contains($opdrachtgever->id)) selected @endif>{{$opdrachtgever->getFullName()}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="docenten" class="col-sm-3 form-control-label">{{trans('messages.teacher_s')}}:</label>
                                <div class="col-sm-9">
                                    <select multiple name="docenten[]" id="docenten">
                                        @foreach($teachers as $teacher)
                                            <option value="{{$teacher->id}}" @if($event->participants->contains($teacher->id)) selected @endif>{{$teacher->getFullName()}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label for="address" class="col-xs-3 col-form-label">{{trans('messages.address')}}:</label>
                            <div class="col-xs-5">
                                <input class="form-control" type="text" name="address" value="@if(old('address') == null){{ $event->address }}@else{{old('address')}}@endif" id="address" max="255">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="zipcode" class="col-xs-3 col-form-label">{{trans('messages.zipcode')}}:</label>
                            <div class="col-xs-3">
                                <input class="form-control" type="text" name="zipcode" value="@if(old('zipcode') == null){{ $event->zipcode }}@else{{old('zipcode')}}@endif" id="zipcode" maxlength="10">
                            </div>
                            <label for="city" class="col-xs-1 col-form-label">{{trans('messages.city')}}:</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" name="city" value="@if(old('city') == null){{ $event->city }}@else{{old('city')}}@endif" id="city" maxlength="255">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="terms" class="col-sm-3 form-control-label">{{trans('messages.t_and_c')}}:</label>
                            <div class="col-sm-9">
                                <textarea name="terms" class="form-control" id="terms" rows="7" style="resize: none" maxlength="5000">@if(old('terms') == null){{ $event->terms }}@else{{old('terms')}}@endif</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="briefing" class="col-sm-3 form-control-label">Briefing:</label>
                            <div class="col-sm-9">
                                <textarea name="briefing" class="form-control" id="briefing" rows="7" style="resize: none" maxlength="5000">@if(old('briefing') == null){{ $event->briefing }}@else{{old('briefing')}}@endif</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">{{$button}}</button>
                            </div>
                        </div>
                    </form>
                    @push('scripts')
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $(function () {
                                    $('#startdate').datetimepicker({
                                        format: 'D/MM/YYYY  HH:mm',
                                        icons: {
                                            time:"fa fa-clock-o",
                                            date:"fa fa-calendar",
                                            up:"fa fa-chevron-up",
                                            down:"fa fa-chevron-down",
                                            previous:"fa fa-chevron-left",
                                            next:"fa fa-chevron-right",
                                            today:"fa fa-crosshairs",
                                            clear:"fa fa-trash",
                                            close:"fa fa-times"
                                        }
                                    });
                                    $('#enddate').datetimepicker({
                                        useCurrent: false, //Important! See issue #1075
                                        format: 'D/MM/YYYY  HH:mm',
                                        icons: {
                                            time:"fa fa-clock-o",
                                            date:"fa fa-calendar",
                                            up:"fa fa-chevron-up",
                                            down:"fa fa-chevron-down",
                                            previous:"fa fa-chevron-left",
                                            next:"fa fa-chevron-right",
                                            today:"fa fa-crosshairs",
                                            clear:"fa fa-trash",
                                            close:"fa fa-times"
                                        }
                                    });
                                    $("#startdate").on("dp.change", function (e) {
                                        $('#enddate').data("DateTimePicker").minDate(e.date);
                                    });
                                    $("#enddate").on("dp.change", function (e) {
                                        $('#startdate').data("DateTimePicker").maxDate(e.date);
                                    });
                                });
                                $("#opdrachtgevers").select2({
                                    width: '100%'
                                });
                                $("#docenten").select2({
                                    width: '100%'
                                });
                                if(!$('#terms').val()){
                                    var terms = '{{trans('messages.standard_terms')}}';
                                    $('#terms').val(terms);
                                }
                            });
                        </script>
                    @endpush
                </div>
            </div>
        </div>
    </div>
@endsection