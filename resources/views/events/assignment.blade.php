@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card card-block ">
                    <div class="title-block">
                        <h3 class="title">
                            {{$title}}
                        </h3>
                    </div>
                    <hr>
                    @include('errors.errors')
                    <form action="{{route('event_add_participants',[$event])}}" method="POST">
                        {{ csrf_field() }}
                        <div id="listbox">
                            <select multiple="multiple" name="participants[]">
                                @foreach($people as $person)
                                    @if(!$person->events->contains($event->id))
                                        <option value="{{$person->id}}">
                                            {{$person->getFullName()}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        $('#listbox').bootstrapDualListbox({
                                            moveOnSelect: false,
                                            selectedListLabel: '{{trans('messages.selected_people')}}',
                                            nonSelectedListLabel: '{{trans('messages.available_people')}}',
                                            selectorMinimalHeight: 175
                                        });
                                    });
                                </script>
                            @endpush
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <p><b>Note: </b>{{trans('messages.assignment_note')}}</p>
                            </div>
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">{{trans('messages.add_students')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection