const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
    // Master.css
    mix.styles(
        [
            'bootstrap.css',
            'dataTables.bootstrap4.min.css',
            'bootstrap-duallistbox.min.css',
            'bootstrap-datetimepicker.min.css',
            'select2.min.css',
            'template.css'
        ],
        'public/assets/css/master.css',
        'resources/assets/css/'
    );

    // Master.js
    mix.scripts(
        [
            'jquery-3.1.0.min.js',
            // 'jquery-2.1.4.min.js',
            'jquery-ui.min.js',
            'tether.min.js',
            'bootstrap.js',
            'moment.min.js',
            'jquery.dataTables.min.js',
            'jquery.bootstrap-duallistbox.js', //Did some manual changes in this file ( glyphicon -> font awsome)
            'dataTables.bootstrap4.min.js',
            'bootstrap-toolkit.min.js',
            'bootstrap-datetimepicker.js',
            'select2.min.js',
            'typeahead.bundle.min.js',
            'tinycolor.js',
            'metisMenu.min.js',
            'nprogress.js',
            'template.js'
        ],
        'public/assets/js/master.js',
        'resources/assets/js/'
    );
});