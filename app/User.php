<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laratrust\Traits\LaratrustUserTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;
    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password','created_by', 'updated_by'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'firstname' => 10,
            'lastname' => 10,
            'school_id' => 2,
            'email' => 5,
        ],
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'dateofbirth'];

    public function setEducationalLevelAttribute($value)
    {
        $this->attributes['educational_level'] = (is_array($value)) ? implode(', ', $value) : null;
    }

    /**
     * The user's firstname + lastname_prefix + lastname
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->attributes['firstname'].' '.$this->attributes['lastname_prefix'].' '.$this->attributes['lastname'];
    }

    /**
     * Get string with user's active status(yes/no) and a icon (with link to toggle the status)
     *
     * @return string
     */
    public function getStatusWithToggle()
    {
        $status = ($this->attributes['status'] == 1) ? trans('messages.yes') : trans('messages.no');
        $icon   = ($this->attributes['status'] == 1) ? 'fa-toggle-off' : 'fa-toggle-on';
        $title  = ($this->attributes['status'] == 1) ? trans('messages.deactivate_acc')  : trans('messages.activate_acc');
        return $status . ' (<a href="'.route('account_change_status', [$this->attributes['id']]).'"><i class="fa '.$icon.'" title="'.$title.'" aria-hidden="true"></i></a>)';
    }

    /**
     * The groups that the user belongs to.
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    /**
     * The events that the user is signed up / accepted / declined for.
     */
    public function events()
    {
        return $this->belongsToMany('App\Event')->withPivot('status', 'report_link', 'grade', 'credited_time', 'notes');
    }

    /**
     * tasks assigned to the user
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class);
    }
}