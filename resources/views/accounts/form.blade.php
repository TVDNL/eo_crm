@extends('layouts.master')
@section('title', 'Account '.trans('message.form'))
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card card-block ">
                    <div class="title-block">
                        <h3 class="title">
                            {{$title}}
                        </h3>
                    </div>
                    <br>
                    @include('errors.errors')
                    <form action="{{$route}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if($type == 'manual')
                            <u>{{trans('messages.standard_info')}}</u>
                            <hr>
                            @if(empty($user->firstname))
                                <div class="form-group row">
                                    <label for="type" class="col-xs-2 col-form-label">{{trans('messages.acc_type')}}*:</label>
                                    <div class="col-xs-6">
                                        <label class="custom-control custom-radio">
                                            <input id="radio1" name="role" type="radio" class="custom-control-input" value="opdrachtgever" required @if(old('role') == null) @if($user->hasRole(['opdrachtgever'])) checked @endif @elseif(old('role') == 'opdrachtgever') checked @endif>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.client')}}</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="radio2" name="role" type="radio" class="custom-control-input" value="docent" required @if(old('role') == null) @if($user->hasRole(['docent'])) checked @endif @elseif(old('role') == 'docent') checked @endif>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.teacher')}}</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="radio3" name="role" type="radio" class="custom-control-input" value="leerling" required @if(old('role') == null) @if($user->hasRole(['leerling'])) checked @endif @elseif(old('role') == 'leerling') checked @endif>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.student')}}</span>
                                        </label>
                                    </div>
                                </div>
                            @else
                                <input type="hidden" id="userRole" value="{{$user->roles()->first()->name}}">
                            @endif
                            <div class="form-group row">
                                <label for="firstname" class="col-xs-2 col-form-label">{{trans('messages.name')}}*:</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="firstname" @else readonly @endif value="@if(old('firstname') == null){{ $user->firstname }}@else{{old('firstname')}}@endif" id="firstname" maxlength="255" required>
                                </div>
                                <label for="prefix" class="col-xs-2 col-form-label">{{trans('messages.prefix')}}:</label>
                                <div class="col-xs-1">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="prefix" @else readonly @endif value="@if(old('prefix') == null){{ $user->lastname_prefix }}@else{{old('prefix')}}@endif" id="prefix" maxlength="255">
                                </div>
                                <label for="lastname" class="col-xs-1 col-form-label">{{trans('messages.lastname')}}*:</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="lastname" @else readonly @endif value="@if(old('lastname') == null){{ $user->lastname }}@else{{old('lastname')}}@endif" id="lastname" maxlength="255" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="school_id" class="col-xs-2 col-form-label">School ID:</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="number" @if(Laratrust::hasRole(['docent']))name="id" @else readonly @endif value="@if(old('id') == null){{ $user->school_id }}@else{{old('id')}}@endif" id="school_id" max="9999999">
                                </div>
                                <label for="email" class="col-xs-2 col-form-label">Email*:</label>
                                <div class="col-xs-4">
                                    <input class="form-control" type="email" @if(Laratrust::hasRole(['docent']))name="email" @else readonly @endif value="@if(old('email') == null){{ $user->email }}@else{{old('email')}}@endif" id="email" maxlength="255" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="dateofbirth" class="col-xs-2 col-form-label">{{trans('messages.date_of_birth')}}:</label>
                                <div class="col-xs-3">
                                    {{--todo date doesnt display when used in combination with old(), needs fixing--}}
                                    {{--<input class="form-control" type="date" @if(Laratrust::hasRole(['docent']))name="dateofbirth" @else readonly @endif value="@if(old('dateofbirth') == null) @if(!empty($user->dateofbirth)) {{$user->dateofbirth->format('Y-m-d')}} @endif @else {{old('dateofbirth', date('Y-m-d'))}} @endif" id="dateofbirth">--}}
                                    <input class="form-control" type="date" @if(Laratrust::hasRole(['docent']))name="dateofbirth" @else readonly @endif value="@if(!empty($user->dateofbirth)){{$user->dateofbirth->format('Y-m-d')}}@endif" id="dateofbirth">
                                </div>
                                <label for="phonenumber" class="col-xs-2 col-form-label">{{trans('messages.phonenumber')}}:</label>
                                <div class="col-xs-4">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="phonenumber" @else readonly @endif value="@if(old('phonenumber') == null){{ $user->phonenumber }}@else{{old('phonenumber')}}@endif" id="phonenumber" maxlength="255">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-xs-2 col-form-label">{{trans('messages.address')}}:</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="address" @else readonly @endif value="@if(old('address') == null){{ $user->address }}@else{{old('address')}}@endif" id="address" maxlength="255">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="zipcode" class="col-xs-2 col-form-label">{{trans('messages.zipcode')}}:</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="zipcode" @else readonly @endif value="@if(old('zipcode') == null){{ $user->zipcode }}@else{{old('zipcode')}}@endif" id="zipcode" maxlength="255">
                                </div>
                                <label for="city" class="col-xs-2 col-form-label">{{trans('messages.city')}}:</label>
                                <div class="col-xs-4">
                                    <input class="form-control" type="text" @if(Laratrust::hasRole(['docent']))name="city" @else readonly @endif value="@if(old('city') == null){{ $user->city }}@else{{old('city')}}@endif" id="city" maxlength="255">
                                </div>
                            </div>
                            <br>
                            <div id="extra_information" style="display: none">
                                <u>{{trans('messages.extra_info')}}</u>
                                <hr>
                                <div class="form-group row">
                                    <label for="level" class="col-sm-2 form-control-label">{{trans('messages.edu_level')}}</label>
                                    <div class="col-sm-3">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name='level[]' value="1" min="1" max="3" @if(in_array('1', explode(', ', $user->educational_level))) checked @endif > 1
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name='level[]' value="2" min="1" max="3" @if(in_array('2', explode(', ', $user->educational_level))) checked @endif > 2
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name='level[]' value="3" min="1" max="3" @if(in_array('3', explode(', ', $user->educational_level))) checked @endif > 3
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="allergies" class="col-xs-2 col-form-label">{{trans('messages.allergies')}}:</label>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" name="allergies" value="@if(old('allergies') == null){{ $user->allergies }}@else{{old('allergies')}}@endif" id="allergies" maxlength="255">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="size" class="col-xs-2 col-form-label">{{trans('messages.size')}}:</label>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" name="size" value="@if(old('size') == null){{ $user->size }}@else{{old('size')}}@endif" id="size" maxlength="10">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="calamity" class="col-xs-2 col-form-label">{{trans('messages.emergency_nr')}}:</label>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" name="calamity" value="@if(old('calamity') == null){{ $user->calamity }}@else{{old('calamity')}}@endif" id="calamity" maxlength="255">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bhv" class="col-xs-2 col-form-label"><span class="fa fa-info-circle input_help" title="{{trans('input.ero')}}"></span> {{trans('messages.ero')}}:</label>
                                    <div class="col-xs-6">
                                        <label class="custom-control custom-radio">
                                            <input id="radio4" name="bhv" type="radio" class="custom-control-input" value="1" @if(old('bhv') == null) @if($user->bhv == 1) checked @endif @elseif(old('bhv') == 1) checked @endif required>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.yes')}}</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="radio5" name="bhv" type="radio" class="custom-control-input" value="0" @if(old('bhv') == null) @if($user->bhv == 0) checked @endif @elseif(old('bhv') == 0) checked @endif required>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.no')}}</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="hygiene" class="col-xs-2 col-form-label"><span class="fa fa-info-circle input_help" title="{{trans('input.hygiene')}}"></span> {{trans('messages.hygiene')}}:</label>
                                    <div class="col-xs-6">
                                        <label class="custom-control custom-radio">
                                            <input id="radio4" name="hygiene" type="radio" class="custom-control-input" value="1" @if(old('hygiene') == null) @if($user->hygiene == 1) checked @endif @elseif(old('hygiene') == 1) checked @endif required>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.yes')}}</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input id="radio5" name="hygiene" type="radio" class="custom-control-input" value="0" @if(old('hygiene') == null) @if($user->hygiene == 0) checked @endif @elseif(old('hygiene') == 0) checked @endif required>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{trans('messages.no')}}</span>
                                        </label>
                                    </div>
                                </div>
                                @if(Laratrust::hasRole(['docent']))
                                    <br>
                                    <u>{{trans('messages.notes')}}</u>
                                    <hr>
                                    <p>{{trans('messages.notes_explained')}}</p>
                                    <div class="form-group row">
                                        <div class="col-xs-6">
                                            <textarea name="notes" class="form-control" id="notes" rows="7" style="resize: none" maxlength="2000">@if(old('notes') == null){{ $user->notes }}@else{{old('notes')}}@endif</textarea>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @else
                            <div class="form-group row">
                                <label for="file" class="col-sm-3 form-control-label">
                                    {{trans('messages.excel')}}
                                </label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <span class="btn btn-success">
                                                Browse&hellip; <input id="excel" name="excel" style="display:none;" type="file">
                                            </span>
                                        </label>
                                        <input type="text" class="form-control" id="upload-file-info" readonly>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <br>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-success">
                                    {{$button}}
                                </button>
                            </div>
                        </div>
                    </form>
                    @push('scripts')
                        <script>
                            $(document).ready(function() {
                                if($('#userRole').val() == 'leerling'){
                                    $('#extra_information').css('display', 'block')
                                }
                                if($('#radio3').is(':checked')){
                                    $('#extra_information').css('display', 'block')
                                }
                                $("input[name=role]").on("change", function()
                                {
                                    $(this).each(function(){
                                        if ($(this).is(":checked")) {
                                            if($(this).val() == 'docent' || $(this).val() == 'opdrachtgever'){
                                                $('#extra_information').css('display', 'none')
                                            }else{
                                                $('#extra_information').css('display', 'block')
                                            }
                                        }
                                    });
                                });
                                $('#excel').on('change',function(){
                                    var filename = $(this).val().replace('C:\\fakepath\\', '');
                                    $('#upload-file-info').val(filename);
                                });
                            });
                        </script>
                    @endpush
                </div>
            </div>
        </div>
    </div>
@endsection