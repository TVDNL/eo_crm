<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\StoreGroup;
use App\User;

use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function overview()
    {
        if(!Auth::user()->hasRole(['docent'])) abort(403);

        return view('groups/overview',[
            'groups' => Group::select(['id', 'name', 'description'])->with('members')->get()
        ]);
    }

    public function form(Group $group = null)
    {
        if($group->id == null){
            $title  = trans('messages.create_group');
            $route  = route('group_form_send');
            $button = trans('messages.save');
            $users = User::all();
        }else{
            $title  = trans('messages.edit_group');
            $route  = route('group_form_send', ['event' => $group->id]);
            $button = trans('messages.save');
            $users = User::all();
        }

        return view('groups/form',[
            'group' => $group,
            'title' => $title,
            'route' => $route,
            'button'=> $button,
            'people' => $users
        ]);
    }

    public function store(StoreGroup $request, Group $group = null)
    {
        if($group->id == null){
            $group = new Group();
            $redirect = redirect()->route('group_overview')->with('status', trans('messages.group_suc_created'));
        }else{
            $redirect = redirect()->route('group_overview')->with('status', trans('messages.group_suc_edited'));
        }

        $group->name = $request->name;
        $group->description = $request->description;
        $group->save();

        if(!empty($request->members)){
            $group->members()->sync($request->members);
        }else{
            $group->members()->sync([]);
        }

        return $redirect;
    }

    public function view(Group $group)
    {
        return view('groups.details',[
            'group' => $group
        ]);
    }
}