@extends('layouts.master')
@section('title', 'Groups overview')
@section('content')
    @push('custom-css')
        <style>
            .fa-eye {text-decoration: none; }
            #title a {text-decoration: none; color: #4f5f6f}
            #icons {text-align: center;}
            #icons i {padding-right: 5px}
        </style>
    @endpush
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>{{trans('messages.name')}}</th>
                <th>{{trans('messages.description')}}</th>
                <th>{{trans('messages.members')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($groups as $group)
            <tr>
                <td id="icons">
                    <a title="Edit Group" href="{{route('group_edit', ['group' => $group->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a title="View Group" href="{{route('group_details', ['group' => $group->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                </td>
                <td id="title">
                    <a href="{{route('group_details', ['group' => $group->id])}}">{{$group->name}}</a>
                </td>
                <td>{{$group->description}}</td>
                <td>{{$group->members()->count()}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @push('scripts')
        <script>
            $(document).ready(function() {
                //initialize the datatable
                $('#table').DataTable({
                    "language": {
                        @if(App::getLocale() == 'en')
                        "url": "{{URL::asset('assets/dataTables/en.json')}}"
                        @else
                        "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                        @endif
                    },
                    order: [[1, 'desc']],
                    columns: [
                        //Icons
                        {
                            orderable: false,
                            width: "6%"
                        },
                        //Title
                        null,
                        //Description
                        null,
                        //Members
                        null
                    ]
                });
            } );
        </script>
    @endpush
@endsection