<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('status')->default(1);
            $table->integer('school_id')->unsigned()->nullable()->unique();
            $table->string('firstname')->nullable();
            $table->string('lastname_prefix')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('gender')->nullable();
            $table->string('dateofbirth')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('city')->nullable();
            $table->string('educational_level')->nullable();
            $table->string('phonenumber')->nullable();
            $table->string('allergies')->nullable();
            $table->string('size')->nullable();
            $table->string('calamity')->nullable();
            $table->boolean('hygiene')->nullable();
            $table->boolean('bhv')->default(0);
            $table->string('notes', 2000)->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
            $table->integer('deleted_by')->unsigned()->nullable()->default(null);
            $table->foreign('deleted_by')->references('id')->on('users');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}