<?php
/**
 * Created by PhpStorm.
 * User: Thomas van Dellen
 * Date: 27-1-2017
 * Time: 10:31
 */



//Input fields help text
return [
    'ero' => 'Heb je een geldig certificaat voor Bedrijfs Hulp Verlening?',
    'hygiene' => 'Heb je een geldig certificaat voor Sociale Hygiene?',
    'event_type' => 'Studenten kunnen zich vrijwillig inschrijven voor openbare evenementen. Voor persoonlijke evenementen moeten docenten zelf de leerlingen toevoegen. Ook verschijnen de persoonlijke evenementen niet in de overzicht van studenten als ze er niet aan gekoppeld zijn.'
];