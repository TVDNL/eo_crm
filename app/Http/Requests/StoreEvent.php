<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if($this->user()->hasRole(['docent'])){
            return true;
        }elseif($this->user()->hasRole(['opdrachtgever'])){
            return ($this->event->participants->contains($this->user()->id)) ? true : abort(403);
        }else{
            return abort(403);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'           => 'required|in:public,personal',
            'status'         => 'required|boolean',
            'title'          => 'required|max:255',
            'description'    => 'max:2000',
            'startdate'      => 'required', //currently the form returns a custom format
            'enddate'        => 'required', //
            'total_hours'    => 'time_credit',
            'level'          => 'array|in:1,2,3',
            'min_students'   => 'integer',
            'max_students'   => 'integer',
            'address'        => 'max:255',
            'city'           => 'max:255',
            'zipcode'        => 'max:10',
            'terms'          => 'max:5000',
            'briefing'       => 'max:5000',
            'opdrachtgevers' => 'array',
            'docenten'       => 'array'
        ];
    }
}