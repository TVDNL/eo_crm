@extends('layouts.master')
@section('title', 'Events overview')
@section('content')
    @push('custom-css')
        <style>
            .fa-eye {text-decoration: none; }
            #title a {text-decoration: none; color: #4f5f6f}
            #icons {text-align: center;}
            #icons i {padding-right: 5px}
        </style>
    @endpush
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>Status</th>
                <th>Type</th>
                <th>{{trans('messages.title')}}</th>
                <th>{{trans('messages.edu_level')}}</th>
                <th>Start</th>
                <th>{{trans('messages.created_by')}}</th>
                <th>{{trans('messages.created_at')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($events as $event)
                <tr>
                    <td id="icons">
                        @if(Laratrust::hasRole(['admin', 'docent', 'opdrachtgever']))
                            <a title="Edit Event" href="{{route('event_edit', ['event' => $event->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        @endif
                        <a title="View Event" href="{{route('event_details', ['event' => $event->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    </td>
                    <td>{{$event->status}}</td>
                    <td>{{$event->type}}</td>
                    <td id="title">
                        <a href="{{route('event_details', ['event' => $event->id])}}">{{$event->title}}</a>
                    </td>
                    <td>{{$event->educational_level}}</td>
                    <td>{{$event->startDate}}</td>
                    <td>{{$event->creator->getFullName()}}</td>
                    <td>{{$event->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @push('scripts')
        <script>
            $(document).ready(function() {
                //initialize the datatable
                $('#table').DataTable({
                    "language": {
                        @if(App::getLocale() == 'en')
                        "url": "{{URL::asset('assets/dataTables/en.json')}}"
                        @else
                        "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                        @endif
                    },
                    order: [[3, 'desc']],
                    columns: [
                        //Icons
                        {
                            orderable: false,
                            width: "6%"
                        },
                        //status
                        null,
                        //Type
                        null,
                        //Title
                        null,
                        //Level
                        {
                            width: "8%"
                        },
                        //Startdate
                        {
                            render: function(data){
                                var date = moment(data);
                                return date.format('DD/MM/YYYY kk:ss');
                            }
                        },
                        // Created by
                        null,
                        //Created at
                        {
                            render: function(data){
                                var date = moment(data);
                                return date.format('DD/MM/YYYY kk:ss');
                            }
                        }
                    ]
                });
            } );
        </script>
    @endpush
@endsection