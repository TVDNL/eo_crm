<?php
/**
 * Created by PhpStorm.
 * User: Thomas van Dellen
 * Date: 17-1-2017
 * Time: 10:32
 */


//Input fields help text
return [
    'ero' => 'Do you have a valid certificate for Emergency Response Officer?',
    'hygiene' => 'Do you have a valid certificate for Social Hygiene?',
    'event_type' => 'Students can voluntarily sign up for public events.
    While for personal events teachers have to assign them. Also personal events wont show up in the student overview if they aren\'t assigned.'
];