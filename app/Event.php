<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Event extends Model
{
    use SearchableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'endDate', 'startDate', 'created_by', 'updated_by'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'description' => 8,
        ],
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['endDate', 'startDate', 'deleted_at'];

    public function setEducationalLevelAttribute($value)
    {
        $this->attributes['educational_level'] = (is_array($value)) ? implode(', ', $value) : null;
    }

    public function getTypeAttribute($value)
    {
//        todo fix in event create
//        if($this->attributes['type'] == 'public'){
//            return trans('messages.public');
//        }
//        if($this->attributes['type'] == 'personal'){
//            return trans('messages.personal');
//        }
        return $this->attributes['type'] = ucfirst($value);
    }

    public function getStatusAttribute($value)
    {
        return ($value == 1) ? trans('messages.active') : trans('messages.hidden');
    }

    /**
     * Event creator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * The users that are assigned to the event.
     */
    public function participants()
    {
        return $this->belongsToMany('App\User')->withPivot('status', 'report_link','created_at', 'credited_time', 'grade');
    }

    /**
     * Tasks assigned to the event
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * Get string with the whole address (address, zipcode, city)
     *
     * @return string
     */
    public function getFullAddress()
    {
        $fullAddress = [];
        if(!empty($this->attributes['address'])){
            array_push($fullAddress, $this->attributes['address']);
        }
        if(!empty($this->attributes['zipcode'])){
            array_push($fullAddress, $this->attributes['zipcode']);
        };
        if(!empty($this->attributes['city'])){
            array_push($fullAddress, $this->attributes['city']);
        };

        return implode(', ', $fullAddress);
    }
}