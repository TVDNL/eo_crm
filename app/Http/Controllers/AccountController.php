<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\StoreAccountCreation;
use App\Http\Requests\StoreAccountUpdate;
use App\Mail\SendAccountDetails;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class AccountController extends Controller
{
    //The excel columns
    //ID	Huisadres	Postcode	Plaats	Geslacht	Roepnaam	Tussenvoegsel	Achternaam	Telefoon	Geboortedatum	E-mailadres
    private $columns = ['id', 'huisadres', 'postcode', 'plaats', 'geslacht', 'roepnaam',
        'tussenvoegsel', 'achternaam', 'telefoon', 'geboortedatum', 'e-mailadres'];

    public function overview()
    {
        return view('accounts/overview',[
            'users' => User::all()
        ]);
    }

    public function form($type = '', User $user = null){
        if($type == 'manual'){
            if($user->id == null){
                //Students may not create new accounts
                if(Auth::user()->hasRole(['leerling'])) abort(403);
                $title = trans('messages.create_new_user');
                $button = trans('messages.create_account_email');
                $route = route('account_form_send', [$type]);
                $user = new User();
            }else{
                //Student may only edit his own profile
                if(Auth::user()->hasRole(['leerling']) && Auth::id() != $user->id) abort(403);
                $title = trans('messages.edit_user');
                $button = trans('messages.save_changes');
                $route = route('account_form_send_edit', [$user]);
            }
        }elseif($type == 'automatic'){
            //Students may not upload excel files
            if(Auth::user()->hasRole(['leerling'])) abort(403);
            $title = trans('messages.create_new_students');
            $button = trans('messages.upload');
            $route = route('account_preview');
            $user = new User();
        }else{
            abort(404);
        }

        return view('accounts/form',[
            'type' => $type,
            'title' => $title,
            'route' => $route,
            'button'=> $button,
            'user' => $user
        ]);
    }

    public function edit(StoreAccountUpdate $request, User $user)
    {
        if(Auth::user()->hasRole(['docent'])){
            $user->school_id         = (empty($request->id)) ? null : $request->id;
            $user->firstname         = $request->firstname;
            $user->lastname_prefix   = (empty($request->prefix)) ? null : $request->prefix;
            $user->lastname          = $request->lastname;
            $user->email             = $request->email;
            $user->dateofbirth       = (empty($request->dateofbirth)) ? null : $request->dateofbirth;
            $user->address           = (empty($request->address)) ? null : $request->address;
            $user->zipcode           = (empty($request->zipcode)) ? null : $request->zipcode;
            $user->city              = (empty($request->city)) ? null : $request->city;
            $user->phonenumber       = (empty($request->phonenumber)) ? null : $request->phonenumber;
            $user->notes             = (empty($request->notes)) ? null : $request->notes;
            $user->educational_level = (empty($request->level)) ? null : $request->level;
        }
        $user->allergies       = (empty($request->allergies)) ? null : $request->allergies;
        $user->size            = (empty($request->size)) ? null : $request->size;
        $user->calamity        = (empty($request->calamity)) ? null : $request->calamity;
        $user->hygiene         = $request->hygiene;
        $user->bhv             = $request->bhv;
        $user->updated_by      = Auth::id();

        $user->save();

        return redirect()->route('profile', [$user])->with('status', trans('messages.acc_suc_updated'));
    }

    public function store(StoreAccountCreation $request, $type, User $user = null){
        if($type == 'automatic'){
            if(isset($request->name) && !empty($request->name)){
                $group = new Group();
                $group->name = $request->name;
                $group->description = $request->description;
                $group->save();
            }
            $excel = $request->session()->get('latest_excel', redirect()->route('home'));
            foreach ($excel as $key => $value) {
                if(in_array($value->id, $request->users)){
                    $user = new User();
                    $user->school_id         = (empty($value->id)) ? null : $value->id;
                    $user->firstname         = $value->roepnaam;
                    $user->lastname_prefix   = (empty($value->tussenvoegsel)) ? null : $value->tussenvoegsel;
                    $user->lastname          = $value->achternaam;
                    $user->email             = (empty($value->e_mailadres)) ? null : $value->e_mailadres;
                    $user->gender            = (empty($value->geslacht)) ? null : $value->geslacht;
                    $user->dateofbirth       = (empty($value->geboortedatum)) ? null : $value->geboortedatum;
                    $user->address           = (empty($value->huisadres)) ? null : $value->huisadres;
                    $user->zipcode           = (empty($value->postcode)) ? null : $value->postcode;
                    $user->city              = (empty($value->plaats)) ? null : $value->plaats;
                    $user->phonenumber       = (empty($value->telefoon)) ? null : $value->telefoon;

                    $user->educational_level = (empty($value->level)) ? null : $value->level;

                    $pw = str_random(8);
                    $user->password          = Hash::make($pw);
                    $user->created_by        = Auth::id();
                    $user->updated_by        = Auth::id();
                    $user->save();

                    $user->attachRole(\App\Role::where('name', '=', 'leerling')->first());

                    if(isset($group)) $group->members()->attach($user->id);

                    Mail::to($user->email)->send(new SendAccountDetails($user, $pw));
                }
            }
            return redirect()->route('home')->with('status', trans('messages.acc_suc_created'));
        }else{
            $user = new User();
            $user->school_id         = (empty($request->id))?null:$request->id;
            $user->firstname         = $request->firstname;
            $user->lastname_prefix   = (empty($request->prefix)) ? null : $request->prefix;
            $user->lastname          = $request->lastname;
            $user->email             = $request->email;
            $user->dateofbirth       = (empty($request->dateofbirth)) ? null : $request->dateofbirth;
            $user->address           = (empty($request->address)) ? null : $request->address;
            $user->zipcode           = (empty($request->zipcode)) ? null : $request->zipcode;
            $user->city              = (empty($request->city)) ? null : $request->city;
            $user->phonenumber       = (empty($request->phonenumber)) ? null : $request->phonenumber;
            $user->allergies         = (empty($request->allergies)) ? null : $request->allergies;
            $user->size              = (empty($request->size)) ? null : $request->size;
            $user->calamity          = (empty($request->calamity)) ? null : $request->calamity;
            $user->hygiene           = $request->hygiene;
            $user->bhv               = $request->bhv;
            $user->notes             = (empty($request->notes)) ? null : $request->notes;
            $user->educational_level = (empty($request->level)) ? null : $request->level;
            $pw = str_random(8);
            $user->password        = Hash::make($pw);
            $user->created_by      = Auth::id();
            $user->updated_by      = Auth::id();
            $user->save();

            $user->attachRole(\App\Role::where('name', '=', $request->role)->first());

            Mail::to($user->email)->send(new SendAccountDetails($user, $pw));

            return redirect()->route('account_form', ['manual'])->with('status', trans('messages.acc_suc_created'));
        }
    }

    public function preview(Request $request)
    {
        $this->validate($request, [
            'excel' => 'bail|required|file|mimes:xls,xlsx,csv|student_excel'
        ]);
        $students = [];
        $data = Excel::load($request->excel->path(), function($reader){})->get();
        $request->session()->put('latest_excel', $data);
        foreach ($data as $key => $value) {
            $user = User::where('email', $value->e_mailadres)->orWhere('school_id', $value->id)->first();
            $user_check = (is_null($user)) ? 'acc_not_exist' : 'acc_exists';
            $student = [
                $user_check,
                $value->id,
                $value->roepnaam,
                $value->tussenvoegsel,
                $value->achternaam,
                $value->e_mailadres,
                $value->geslacht,
                $value->geboortedatum,
                $value->huisadres,
                $value->postcode,
                $value->plaats,
                $value->telefoon
            ];

            array_push($students, $student);
        }

        return view('accounts/preview',[
            'title' => 'Preview of uploaded students',
            'students' => $students
        ]);
    }

    public function viewProfile(User $user)
    {
        if(!Auth::user()->hasRole(['docent']) && $user->hasRole(['docent', 'opdrachtgever'])){
            //Nobody except a docent may see docent and opdrachtgever profiles
            abort(403);
        }elseif(Auth::user()->hasRole(['opdrachtgever'])){
            //Opdrachtgevers may only see student profiles that participate in their events
            $events = Auth::user()->events()->get();
            $continue = false;
            foreach ($events as $event){
                if($event->participants->contains($user)) $continue = true;
            }
            if(!$continue) abort(403);

            return view('accounts/details',[
                'user' => $user
            ]);
        }elseif(Auth::user()->hasRole(['leerling']) && !Auth::id() == $user->id){
            //Students dont have too see profiles other than there own
            abort(403);
        }else{
            return view('accounts/details',[
                'user' => $user
            ]);
        }
    }

    public function changeUserStatus(User $user)
    {
        $user->status = ($user->status == 1) ? 0 : 1;
        $user->updated_by = Auth::id();
        $user->save();

        return redirect()->route('account_overview')->with('status', trans('messages.acc_status_updated'));
    }
}