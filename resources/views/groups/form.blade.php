@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card card-block ">
                    <div class="title-block">
                        <h3 class="title">
                            {{$title}}
                        </h3>
                    </div>
                    <hr>
                    @include('errors.errors')
                    <form action="{{$route}}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row"> <label for="name" class="col-sm-3 form-control-label">{{trans('messages.name')}}:</label>
                            <div class="col-sm-9"> <input name="name" type="text" class="form-control" id="name" value="{{$group->name}}"> </div>
                        </div>
                        <div class="form-group row"> <label for="description" class="col-sm-3 form-control-label">{{trans('messages.description')}}:</label>
                            <div class="col-sm-9"> <input name='description' type="text" class="form-control" id="description" value="{{$group->description}}"> </div>
                        </div>
                        <br>
                        <div id="listbox">
                            <select multiple="multiple" name="members[]">
                                @foreach($people as $person)
                                    @if($person->hasRole(['leerling', 'docent']))
                                        <option value="{{$person->id}}" @if($person->groups->contains($group->id)) selected @endif>
                                            {{$person->getFullName()}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        $('#listbox').bootstrapDualListbox({
                                            moveOnSelect: false,
                                            selectedListLabel: '{{trans('messages.selected_people_group')}}',
                                            nonSelectedListLabel: '{{trans('messages.available_people_group')}}',
                                            selectorMinimalHeight: 175
                                        });
                                    });
                                </script>
                            @endpush
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10"> <button type="submit" class="btn btn-success">{{$button}}</button> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection