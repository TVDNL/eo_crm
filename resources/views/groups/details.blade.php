@extends('layouts.master')
@section('title', 'Group details')
@section('content')
    @push('custom-css')
        <style>
            .details {margin-bottom: 66px}
        </style>
    @endpush
    <div class="container">
        <div class="title-block">
            <h3 class="title">
                {{trans('messages.group')}} details <span class="sparkline bar" data-type="bar"></span>
            </h3> </div>
        <div class="details">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.name')}}:
                </label>
                <div> {{$group->name}} </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.description')}}:
                </label>
                <div> {{$group->description}} </div>
            </div>
        </div>
        <div class="title-block">
            <h5 >
                {{trans('messages.group_members')}} <span class="sparkline bar" data-type="bar"></span>
            </h5>
        </div>
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th>{{trans('messages.name')}}</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                @foreach($group->members()->get() as $member)
                    <tr>
                        <td id="icons">
                            {{--<a title="Edit Event" href="{{route('event_edit', ['event' => $event->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>--}}
                            {{--<a title="View Event" href="{{route('event_details', ['event' => $event->id])}}"><i class="fa fa-eye" aria-hidden="true"></i></a>--}}
                        </td>
                        <td>{{$member->getFullName()}}</td>
                        <td>{{$member->email}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    //initialize the datatable
                    $('#table').DataTable({
                        "language": {
                            @if(App::getLocale() == 'en')
                            "url": "{{URL::asset('assets/dataTables/en.json')}}"
                            @else
                            "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                            @endif
                        },
                        columns: [
                            //Icons
                            {
                                orderable: false,
                                width: "5%"
                            },
                            //name
                            null,
                            //email
                            null
                        ]
                    });
                });
            </script>
        @endpush
    </div>
@endsection