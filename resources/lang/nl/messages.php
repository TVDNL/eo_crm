<?php

return array (
  'email_body_client' => 'Bij deze ontvangt u de account gegevens om in te kunnen loggen op onze Event manager.\r\nWij zijn blij samen met u events te gaan draaien. Wanneer u bent ingelogd in ons systeem kunt u uw event/events aanpassen en aanvullen waar nodig.',
  'email_body_student' => 'Bij deze ontvangt je de account gegevens om in te kunnen loggen op onze Event manager.\r\n Wanneer je ingelogd bent kan je de evenementen bekijken en inschrijven.',
  'email_end' => 'Met vriendelijke groet,\r\nHet event team van Noorderpoort',
  'standard_terms' => '- Meld je minimaal twee weken van te voren aan.\r\n- Meld je altijd telefonisch af bij ziekte/afwezigheid en probeer zelf voor vervanging te zorgen.\r\n- Ga netjes gekleed.\r\n- Zorg dat je altijd 10 minuten eerder aanwezig bent dan op het rooster staat.\r\n- Neem een pro-actieve houding aan.\r\n- Wees respectvol naar de mensen om je heen.\r\n- Heb plezier!',
  'create_new_user' => 'Maak nieuwe gebruikers account',
  'create_account_email' => 'Maak account en verstuur email',
  'edit_user' => 'Bewerk gebruiker',
  'save_changes' => 'Bewaar veranderingen',
  'create_new_students' => 'Maak nieuwe student account(s)',
  'upload' => 'Upload',
  'acc_suc_updated' => 'Account succesvol geupdated.',
  'accs_suc_created' => 'Account(s) succesvol aangemaakt.',
  'acc_suc_created' => 'Account succesvol aangemaakt.',
  'acc_status_updated' => 'Account status succesvol geupdated.',
  'create_event' => 'Maak een event',
  'save' => 'Opslaan',
  'edit_event' => 'Evenement aanpassen',
  'event_suc_created' => 'Evenement succesvol aangemaakt.',
  'event_suc_edited' => 'Evenement succesvol aangepast.',
  'add_students' => 'Voeg student(en) toe',
  'students_suc_added' => 'Student(en) succesvol toegevoegd aan de evenement:',
  'suc_signed' => 'Je hebt je succesvol ingeschreven voor de evenement:',
  'suc_removed' => 'Je bent succesvol verwijdered van de evenement:',
  'status_suc_updated' => 'Status succesvol geupdated.',
  'url_suc_updated' => 'Url succesvol geupdated.',
  'grade_suc_updated' => 'Beoordeling van de student succesvol geupdated.',
  'note_suc_updated' => 'Notitie van de student succesvol geupdated.',
  'create_group' => 'Maak een groep',
  'edit_group' => 'Groep aanpassen',
  'group_suc_created' => 'Groep succesvol aangemaakt.',
  'group_suc_edited' => 'Groep succesvol aangepast.',
  'create_task_for' => 'Taak aanmaken voor:',
  'create_task' => 'Maak een taak',
  'create_task_duplicate' => 'Maak de taak aan en start een kopie',
  'edit_task' => 'Taak aanpassen',
  'assign_student' => 'Voeg student(en) toe aan de taak:',
  'assignees_suc_updated' => 'Taak deelnemers succesvol aangepast.',
  'task_suc_created' => 'Taak succesvol aangemaakt.',
  'task_suc_edited' => 'Taak succesvol aangepast.',
  'task_suc_deleted' => 'Taak succesvol verwijderd.',
  'monday' => 'Maandag',
  'tuesday' => 'Dinsdag',
  'wednesday' => 'Woensdag',
  'thursday' => 'Donderdag',
  'friday' => 'Vrijdag',
  'saturday' => 'Zaterdag',
  'sunday' => 'Zondag',
  'schedule' => 'rooster',
  'mail_subject' => 'Je account gegevens voor Eventmanager Noorderpoort',
  'yes' => 'Ja',
  'no' => 'Nee',
  'activate_acc' => 'Activeer account',
  'deactivate_acc' => 'Deactiveer account',
  'profile' => 'Profiel',
  'edit_profile' => 'Profiel aanpassen',
  'standard_info' => 'Standaard Informatie',
  'name' => 'Naam',
  'date_of_birth' => 'Geboortedatum',
  'age' => 'Leeftijd',
  'address' => 'Adres',
  'zipcode' => 'Postcode',
  'city' => 'Plaats',
  'extra_info' => 'Extra Informatie',
  'edu_level' => 'Niveau',
  'allergies' => 'Allergieën',
  'size' => 'T-Shirt maat',
  'emergency_nr' => 'Noodnummer',
  'ero' => 'BHV',
  'hygiene' => 'Sociale Hygiene',
  'notes' => 'Notities',
  'notes_explained' => 'Student specifieke notities zijn alleen zichtbaar voor docenten',
  'student_event_overview' => 'Student evenementen overzicht',
  'total_events' => 'Totaal evenementen',
  'public' => 'Openbaar',
  'personal' => 'Persoonlijk',
  'total_time' => 'Totaal verdiende uren',
  'title' => 'Titel',
  'tasks' => 'Taken',
  'grade' => 'Beoordeling',
  'credited_time' => 'Verdiende uren',
  'form' => 'formulier',
  'acc_type' => 'Account type',
  'client' => 'Opdrachtgever',
  'student' => 'Student',
  'teacher' => 'Docent',
  'prefix' => 'Tussenvoegsel',
  'lastname' => 'Achternaam',
  'phonenumber' => 'Telefoonnummer',
  'excel' => 'Excel bestand met student informatie',
  'required' => 'verplicht',
  'active' => 'Actief',
  'role' => 'Rol',
  'gender' => 'Geslacht',
  'option_group' => '(optioneel) Maak een groep en koppel die aan de studenten',
  'description' => 'Omschrijving',
  'create_accs_email' => 'Maak account(s) en verstuur email',
  'select_1_student' => 'Selecteer tenminste 1 student',
  'user_exist' => 'Gebruiker met deze email of id bestaat al',
  'reset_pw' => 'Reset Wachtwoord',
  'send_reset_link' => 'Verstuur Wachtwoord Reset Link',
  'password' => 'Wachtwoord',
  'confirm_pw' => 'Bevestig Wachtwoord',
  'forgot_pw' => 'Wachtwoord vergeten?',
  'remember_me' => 'Onthoudt mij',
  'access_denied' => 'Toegang Geweigerd.',
  'back_to_homepage' => 'Terug naar de homepagina',
  'sorry_not_found' => 'Sorry, pagina niet gevonden',
  'brb' => 'We zijn zo terug!',
  'add_student_form' => 'Student(en) toevoegen formulier',
  'selected_people' => 'Geselecteerde studenten',
  'available_people' => 'Beschikbare studenten',
  'assignment_note' => 'De geselecteerde studenten krijgen automatisch de geaccepteerd status',
  'event' => 'Evenement',
  'client_s' => 'Opdrachtgever(s)',
  'teacher_s' => 'Docent(en)',
  'students' => 'Studenten',
  'total_hours' => 'Totaal uren',
  'date' => 'Datum',
  'location' => 'Locatie',
  'application' => 'Inchrijving',
  'accepted' => 'Geaccepteerd',
  'assigned_tasks' => 'Toegekende taken',
  'report_url' => 'Verslag Url',
  'edit' => 'Bewerk',
  'enter_link' => 'Vul in link',
  'pending' => 'in behandeling',
  'declined' => 'Afgewezen',
  'not_signed' => 'Niet ingeschreven',
  'sign_up' => 'Inschrijven',
  'sign_up_closed' => 'Inschrijving gesloten',
  'manual_add' => 'Student(en) toevoegen',
  'action' => 'Actie',
  'group_s' => 'Groep(en)',
  'signed_at' => 'Ingeschreven op',
  'report_link' => 'Verslag link',
  'user_removal_confirm' => 'Weet je het zeker? De student wordt ook verwijderd van alle taken die gekoppeld zijn aan deze event.',
  'add_new_task' => 'Voeg nieuwe taak toe',
  'end' => 'Eind',
  'no_student_found' => 'Geen studenten gevonden.',
  'no_tasks_found' => 'Geen taken gevonden.',
  'total' => 'Totaal',
  'grade_modal_note' => 'Hier kan je de beoordeling en urental van de student toekennen.',
  'report' => 'Verslag',
  'student_assigned_tasks' => 'De toegekende taken van de student',
  'time_placeholder' => 'in uren (per 0,5)',
  'close' => 'Sluiten',
  'notes_modal_desc1' => 'Hier kan je student specifieke notities plaatsen voor alleen dit event.',
  'notes_modal_desc2' => 'Alleen zichtbaar voor de docenten en opdrachtgevers.',
  'event_report' => ' Evenement Verslag',
  'report_modal_desc' => 'Hier kan je een link invullen waar we je verslag kunnen vinden. (dropbox/google drive etc.)',
  't_and_c' => 'Algemene Voorwaarden',
  'no_terms_found' => 'Geen voorwaarden gevonden',
  'sign_me_up' => 'Schrijf me in',
  'accept_terms' => 'Bij het inschrijven voor deze evenement ga ik akkoord met de algemene voorwaarden.',
  'hidden' => 'Verborgen',
  'number_students' => 'Aantal studenten',
  'created_by' => 'Gemaakt door',
  'created_at' => 'Gemaakt op',
  'group' => 'Groep',
  'group_members' => 'Groeps leden',
  'selected_people_group' => 'Geselecteerde mensen',
  'available_people_group' => 'Beschikbare mensen',
  'members' => 'Leden',
  'search' => 'Zoek...',
  'events' => 'Evenementen',
  'overview' => 'Overzicht',
  'events_manager' => 'Evenementen Beheer',
  'create_new_event' => 'Nieuw evenement',
  'new_acc' => 'Nieuw account',
  'acc_manager' => 'Account Beheer',
  'groups_manager' => 'Groep Beheer',
  'new_group' => 'Nieuwe group',
  'dear' => 'Beste',
  'add_student_s' => 'Voeg student(en) toe',
  'task_note1' => 'Deze taak kan max.',
  'task_note2' => 'student(en) toegekend krijgen.',
  'task' => 'Taak',
  'credit' => 'Uren',
  'assigned_students' => 'Toegekende studenten',
  'assignees' => 'Aantal personen',
  'assign_student_s' => 'Voeg student(en) toe',
  'delete_task' => 'Verwijder taak',
  'export_excel' => 'Exporteer naar excel',
  'my_overview' => 'Mijn Overzicht',
  'my_events' => 'Mijn Evenementen',
  'outstanding_reviews' => 'Mijn openstaande beoordelingen',
  'task_confirm_delete' => 'Weet je zeker dat je deze taak wilt verwijderen?',
);