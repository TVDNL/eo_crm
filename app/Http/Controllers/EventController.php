<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\StoreEvent;
use App\Task;
use App\User;
use DateTime;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function overview()
    {
        if(Auth::user()->hasRole(['opdrachtgever'])){
            $events = Event::whereHas('participants', function($query){
                          $query->where('id', '=', Auth::id());
                      })
                      ->with('creator')->get();
        }elseif(Auth::user()->hasRole(['leerling'])){
            $events = Event::with('creator')->where(['type' => 'public', 'status' => '1'])->orWhere(function($q){
                $q->whereHas('participants', function($query){
                    $query->where('id', '=', Auth::id());
                });
            })->get();
        }else{
            $events = Event::with('creator')->get();
        }

        return view('events/overview',[
            'events' => $events
        ]);
    }

    public function form(Event $event = null)
    {
        if($event->id == null){
            $title  = trans('messages.create_event');
            $route  = route('event_form_send');
            $button = trans('messages.save');
        }else{
            $title  = trans('messages.edit_event');
            $route  = route('event_form_send', ['event' => $event->id]);
            $button = trans('messages.save');
        }

        $opdrachtgevers = User::whereHas('roles', function($q){
            $q->where('name', 'opdrachtgever');
        })->get(['id', 'firstname', 'lastname_prefix', 'lastname']);
        $teachers = User::whereHas('roles', function($q){
            $q->where('name', 'docent');
        })->get(['id', 'firstname', 'lastname_prefix', 'lastname']);

        return view('events/form',[
            'event' => $event,
            'title' => $title,
            'route' => $route,
            'button'=> $button,
            'opdrachtgevers' => $opdrachtgevers,
            'teachers' => $teachers
        ]);
    }

    public function store(StoreEvent $request, Event $event)
    {
        if($event->id == null){
            //Store new event
            $event = new Event();
            $event->created_by = Auth::id();
            $redirect = redirect()->route('event_overview')->with('status', trans('messages.event_suc_created'));
        }else{
            //Store edited event
            if(Auth::user()->hasRole(['admin', 'docent'])) {
                $event->participants()->detach(
                    User::whereHas('roles', function ($q) {
                        $q->where('name', 'opdrachtgever');
                    })->get()
                );
                $event->participants()->detach(
                    User::whereHas('roles', function ($q) {
                        $q->where('name', 'docent');
                    })->get()
                );
            }
            $redirect = redirect()->route('event_details', [$event])->with('status', trans('messages.event_suc_edited'));
        }

        //Save event
        $event->type              = $request->type;
        $event->status            = $request->status;
        $event->title             = $request->title;
        $event->description       = $request->description;
        $event->startDate         = DateTime::createFromFormat('d/m/Y  H:i', $request->startdate);
        $event->endDate           = DateTime::createFromFormat('d/m/Y  H:i', $request->enddate);
        $event->total_hours       = $request->total_hours;
        $event->educational_level = $request->level;
        $event->min_users         = $request->min_students;
        $event->max_users         = $request->max_students;
        $event->address           = $request->address;
        $event->city              = $request->city;
        $event->zipcode           = $request->zipcode;
        $event->terms             = $request->terms;
        $event->briefing          = $request->briefing;
        $event->updated_by        = Auth::id();
        $event->save();

        if(Auth::user()->hasRole(['admin', 'docent'])){
            //Attach "opdrachtgevers"
            if(isset($request->opdrachtgevers)) {
                $opdrachtgevers = (is_array($request->opdrachtgevers)) ? $request->opdrachtgevers : [$request->opdrachtgevers];
                $pivotData = array_fill(0, count($opdrachtgevers), ['status' => 'accepted']);
                $syncDataOpdrachtgevers = array_combine($opdrachtgevers, $pivotData);
                $event->participants()->attach($syncDataOpdrachtgevers);
            }
            //Attach "docenten"
            if(isset($request->docenten)) {
                $docenten = (is_array($request->docenten)) ? $request->docenten : [$request->docenten];
                $pivotData = array_fill(0, count($docenten), ['status' => 'accepted']);
                $syncDataDocenten = array_combine($docenten, $pivotData);
                $event->participants()->attach($syncDataDocenten);
            }
        }
        return $redirect;
    }

    public function view(Event $event)
    {
        //Clients may only see events they are assigned to
        if(Auth::user()->hasRole(['opdrachtgever']) && !$event->participants->contains(Auth::id())) abort(403);
        //Students may only see public events OR when they are attached to an event
        if(Auth::user()->hasRole(['leerling'])){
            if($event->type == 'Personal' && !$event->participants->contains(Auth::id())) abort(403);
            if($event->status == 'Hidden' && !$event->participants->contains(Auth::id())) abort(403);
        };
        $students = [];
        $clients = [];
        $teachers = [];
        foreach($event->participants()->get() as $participant){
            if($participant->pivot->status == 'accepted' && $participant->hasRole('leerling')){
                array_push($students, $participant);
            }
            if($participant->pivot->status == 'accepted' && $participant->hasRole('opdrachtgever')){
                array_push($clients, $participant->getFullName());
            }
            if($participant->pivot->status == 'accepted' && $participant->hasRole('docent')){
                array_push($teachers, $participant->getFullName());
            }
        };
        if(Auth::user()->hasRole(['leerling'])){
            $tasks = $event->tasks()->wherehas('assignees', function($q){
                $q->where('id', Auth::id());
            })->get();
        }else{
            $tasks = $event->tasks()->get();
        }

        return view('events.details',[
            'event'    => $event,
            'students' => $students,
            'clients'  => $clients,
            'teachers' => $teachers,
            'tasks'    => $tasks
        ]);
    }

    public function manualAssignment(Event $event)
    {
        return view('events.assignment', [
           'event' => $event,
           'people' => User::all(),
           'title' => trans('messages.add_students')
        ]);
    }

    public function addParticipants(Request $request, Event $event)
    {
        $users     = $request->participants;
        $pivotData = array_fill(0, count($users), ['status' => 'accepted']);
        $syncData  = array_combine($users, $pivotData);

        $event->participants()->attach($syncData);

        return redirect()->route('event_details', [$event->id])->with('status', trans('messages.students_suc_added').' '.$event->title);
    }

    public function signup(Request $request)
    {
        if(!Auth::user()->hasRole(['leerling'])) abort(403);

        $event = Event::findOrFail($request->event);
        $event->participants()->attach(Auth::id(), ['status' => 'signed_up']);

        return redirect()->route('event_details', [$event->id])->with('status', trans('messages.suc_signed').' '.$event->title);
    }

    public function removeSignup(Event $event){
        if(!Auth::user()->hasRole(['leerling'])) abort(403);

        $event->participants()->detach(Auth::id());

        return redirect()->route('event_details', [$event->id])->with('status', trans('messages.suc_removed').' '.$event->title);
    }

    public function participantStatus(Event $event, User $user, $status)
    {
        $event->participants()->updateExistingPivot($user->id, ['status' => $status]);

        //When a user gets declined, this could also mean he is removed from an event.
        //In that case also remove user from all tasks of that event.
        if($status == 'declined'){
            $tasks = Task::whereHas('assignees', function($query) use ($user, $event){
                $query->where('user_id', '=', $user->id)->where('event_id', '=', $event->id);
            })->get();
            $user->tasks()->detach($tasks);
        }

        return redirect()->route('event_details', [$event->id])->with('status', trans('messages.status_suc_updated'));
    }

    public function storeReportlink(Request $request, Event $event)
    {
        if(!Auth::user()->hasRole(['leerling'])) abort(403);

        $this->validate($request, [
            'report_link' => 'url'
        ]);

        $event->participants()->updateExistingPivot(Auth::id(), ['report_link' => $request->report_link]);

        return redirect()->route('event_details', [$event->id])->with('status', trans('messages.url_suc_updated'));
    }

    public function getGradeInfo(Request $request)
    {
        $user = User::findOrFail($request->user);
        $event = Event::findOrFail($request->event);
        $data = [];
        $tasks = Task::whereHas('assignees', function($query) use ($user, $event){
            $query->where('user_id', '=', $user->id)->where('event_id', '=', $event->id);
        })->get();

        $data['tasks'] = [];
        foreach($tasks as $task){
            array_push($data['tasks'], [$task->title, $task->credit]);
        }

        $data['route'] = route('event_store_grades');
        $data['report_link'] = $user->events()->find($event->id)->pivot->report_link;
        $data['grade'] = $user->events()->find($event->id)->pivot->grade;
        $data['credited_time'] = $user->events()->find($event->id)->pivot->credited_time;

        return $data;
    }

    public function storeGrades(Request $request)
    {
        $this->validate($request, [
            'grade' => 'grade',
            'time'  => 'time_credit'
        ]);

        User::find($request->student)->events()->sync([$request->event => ['grade' => $request->grade, 'credited_time' => (!empty($request->time))?$request->time:0]], false);

        return redirect()->route('event_details', [$request->event])->with('status', trans('messages.grade_suc_updated'));
    }

    public function getStudentNote(Request $request)
    {
        $user = User::findOrFail($request->user);
        $event = Event::findOrFail($request->event);

        $data = ['route' => route('event_store_student_note'), 'notes' => $user->events()->find($event->id)->pivot->notes];

        return $data;
    }

    public function StoreStudentNote(Request $request)
    {
        User::findOrFail($request->student)->events()->sync([$request->event => ['notes' => $request->notes]], false);

        return redirect()->route('event_details', [$request->event])->with('status', trans('messages.note_suc_updated'));
    }
}