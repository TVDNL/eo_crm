@extends('layouts.master')
@section('title', 'Events details')
@section('content')
    @push('custom-css')
        <style>
            .details {margin-bottom: 66px}
            .margin-bottom {margin-bottom: 0px}
            .icon-table {margin-right: 5px}
        </style>
    @endpush
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @include('errors.errors')
        <div class="title-block">
            <h3 class="title">
                {{trans('messages.event')}} details <span class="sparkline bar" data-type="bar"></span>
                @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                    <a href="{{route('event_edit', ['event' => $event->id])}}" class="btn btn-sm btn-primary">{{trans('messages.edit_event')}}</a>
                @endif
            </h3>
        </div>
        <div class="details row">
            <div class="col-sm-6">
                @if(!Laratrust::hasRole(['leerling']))
                    <div class="form-group row margin-bottom">
                        <label class="col-sm-3 form-control-label text-xs-right">
                            Status:
                        </label>
                        <div class="col-sm-9">
                            {{$event->status}}
                        </div>
                    </div>
                @endif
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        Type:
                    </label>
                    <div class="col-sm-9">
                        {{$event->type}}
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.title')}}:
                    </label>
                    <div class="col-sm-9">
                        {{$event->title}}
                    </div>
                </div>

                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.client_s')}}:
                    </label>
                    <div class="col-sm-9">
                        {{implode(', ', $clients)}}
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.teacher_s')}}:
                    </label>
                    <div class="col-sm-9">
                        {{implode(', ', $teachers)}}
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.students')}}:
                    </label>
                    <div class="col-sm-9">
                        {{count($students)}} (Min:{{$event->min_users}} Max:{{$event->max_users}})
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.edu_level')}}:
                    </label>
                    <div class="col-sm-9">
                        {{$event->educational_level}}
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.total_hours')}}:
                    </label>
                    <div class="col-sm-9">
                        {{$event->total_hours}}
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.date')}}:
                    </label>
                    <div class="col-sm-9">
                        {{$event->startDate->format('d/m/Y  H:i')}}  -  {{$event->endDate->format('d/m/Y  H:i')}}
                    </div>
                </div>
                <div class="form-group row margin-bottom">
                    <label class="col-sm-3 form-control-label text-xs-right">
                        {{trans('messages.location')}}:
                    </label>
                    <div class="col-sm-9">
                        {{$event->getFullAddress()}}
                    </div>
                </div>
                @if(Laratrust::hasRole(['leerling']))
                    @if(Auth::user()->events->contains($event->id) && Auth::user()->events()->find($event->id)->pivot->status == 'accepted')
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.application')}} status:
                            </label>
                            <div class="col-sm-9">
                                {{trans('messages.accepted')}}
                            </div>
                        </div>
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.assigned_tasks')}}:
                            </label>
                            <div class="col-sm-9">
                                {{Auth::user()->tasks->count()}}
                            </div>
                        </div>
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.report_url')}}:
                            </label>
                            <div class="col-sm-9">
                                @if(Auth::user()->events->contains($event->id) && Auth::user()->events()->find($event->id)->pivot->status == 'accepted')
                                    {{--todo decide that if you can remove yourself after being accepted as student, or let the teacherrs only remove accepted students--}}
                                    {{--<a href="{{route('event_remove_signup', [$event->id])}}" class="btn btn-primary">Remove yourself from event</a>--}}
                                    @if(!empty(Auth::user()->events()->find($event->id)->pivot->report_link))
                                        <a data-toggle="modal" data-target="#report_modal" class="btn btn-sm btn-primary">{{trans('messages.edit')}}</a>
                                        <a href="{{Auth::user()->events()->find($event->id)->pivot->report_link}}">{{Auth::user()->events()->find($event->id)->pivot->report_link}}</a>
                                    @else
                                        <a data-toggle="modal" data-target="#report_modal" class="btn btn-sm btn-primary">{{trans('messages.enter_link')}}</a>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.grade')}}:
                            </label>
                            <div class="col-sm-9">
                                @if(!empty(Auth::user()->events()->find($event->id)->pivot->grade))
                                    {{Auth::user()->events()->find($event->id)->pivot->grade}}
                                @else
                                    N/A
                                @endif
                            </div>
                        </div>
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.credited_time')}}:
                            </label>
                            <div class="col-sm-9">
                                @if(Auth::user()->events()->find($event->id)->pivot->credited_time >= 1)
                                    {{Auth::user()->events()->find($event->id)->pivot->credited_time}}
                                @else
                                    N/A
                                @endif
                            </div>
                        </div>
                    @elseif(Auth::user()->events->contains($event->id) && Auth::user()->events()->find($event->id)->pivot->status == 'signed_up')
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.application')}} status:
                            </label>
                            <div class="col-sm-9">
                                <a href="{{route('event_remove_signup', [$event->id])}}" class="btn btn-sm btn-primary">Remove application</a>
                                {{trans('messages.application')}} {{trans('messages.pending')}}
                            </div>
                        </div>
                    @elseif(Auth::user()->events->contains($event->id) && Auth::user()->events()->find($event->id)->pivot->status == 'declined')
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.application')}} status:
                            </label>
                            <div class="col-sm-9">
                                {{trans('messages.declined')}}
                            </div>
                        </div>
                    @else
                        <div class="form-group row margin-bottom">
                            <label class="col-sm-3 form-control-label text-xs-right">
                                {{trans('messages.application')}} status:
                            </label>
                            <div class="col-sm-9">
                                @if($event->status == 'Hidden')
                                    {{trans('messages.sign_up_closed')}}
                                @else
                                    {{trans('messages.not_signed')}}
                                    <a data-toggle="modal" data-target="#terms_modal" class="btn btn-sm btn-primary">{{trans('messages.sign_up')}}</a>
                                @endif
                            </div>
                        </div>
                    @endif
                @endif
            </div>
            <div class="w-100"></div>
            <div class="col-sm-6">
                <div class="form-group row margin-bottom">
                    <label class=" form-control-label text-xs-right">
                        {{trans('messages.description')}}:
                    </label>
                    <div>
                        {!!nl2br(e($event->description))!!}
                    </div>
                </div>
                <br>
                <div class="form-group row margin-bottom">
                    <label class=" form-control-label text-xs-right">
                        Briefing:
                    </label>
                    <div>
                        @if(!empty($event->briefing)){!!nl2br(e($event->briefing))!!}@else - @endif
                    </div>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#participants" role="tab">{{trans('messages.students')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tasks" role="tab">{{trans('messages.tasks')}}</a>
            </li>
            <li class="nav-item">
                <a id="schedule" class="nav-link" href="{{route('task_schedule', [$event])}}" >{{ucfirst(trans('messages.schedule'))}}</a>
            </li>
        </ul>
        <br>
        <div class="tab-content">
            <div class="tab-pane active" id="participants" role="tabpanel">
                @if(Laratrust::hasRole(['docent']))
                    <a href="{{route('event_manual_assignment', [$event])}}" class="btn btn-sm btn-primary">{{trans('messages.manual_add')}}</a>
                @endif
                <table id="participants_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                                <th>{{trans('messages.action')}}</th>
                            @endif
                            @if(Laratrust::hasRole(['docent']))
                                <th>Status</th>
                            @endif
                            <th>{{trans('messages.name')}}</th>
                            <th>{{trans('messages.group_s')}}</th>
                            @if(Laratrust::hasRole(['docent', 'admin']))
                                <th>{{trans('messages.credited_time')}}</th>
                                <th>{{trans('messages.grade')}}</th>
                                <th>{{trans('messages.report_link')}}</th>
                                <th>{{trans('messages.signed_at')}}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($event->participants()->get() as $participant)
                            @if($participant->hasRole(['leerling']))
                                <tr>
                                    @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                                        <td>
                                            @if(Laratrust::hasRole(['docent']))
                                                @if($participant->events()->find($event->id)->pivot->status == 'signed_up' || $participant->events()->find($event->id)->pivot->status == 'declined')
                                                    <a title="Accept applicant" href="{{route('event_participant_status', [$event->id, $participant->id, 'accepted'])}}" class="icon-table"><i class="fa fa-check" aria-hidden="true"></i></a>
                                                    @if($participant->events()->find($event->id)->pivot->status == 'signed_up')
                                                        <a title="Decline applicant" href="{{route('event_participant_status', [$event->id, $participant->id, 'declined'])}}" class="icon-table"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                    @endif
                                                @else
                                                    <a title="Remove participant" href="{{route('event_participant_status', [$event->id, $participant->id, 'declined'])}}" class="icon-table" onclick="return confirm({{trans('messages.user_removal_confirm')}})"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                    <a href="#grade_modal" data-toggle="modal" id="{{$participant->id}}" title="{{trans('messages.grade')}} student" class="icon-table"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                @endif
                                            @endif
                                            <a href="#notes_modal" data-toggle="modal" id="{{$participant->id}}" title="Student {{trans('messages.notes')}}" class="icon-table"><i class="fa fa-commenting-o" aria-hidden="true"></i></a>
                                        </td>
                                    @endif
                                    @if(Laratrust::hasRole(['docent']))
                                        <td>{{$participant->pivot->status}}</td>
                                    @endif
                                    @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                                        <td><a href="{{route('profile', [$participant->id])}}" style="text-decoration: none; color: #4f5f6f">{{$participant->getFullName()}}</a></td>
                                    @else
                                        <td>{{$participant->getFullName()}}</td>
                                    @endif
                                    <td>@foreach($participant->groups as $group) {{$group->name}} @if(!$loop->last), @endif @endforeach</td>
                                    @if(Laratrust::hasRole(['docent', 'admin']))
                                        <td style="text-align: center">@if(!empty($participant->pivot->credited_time)){{$participant->pivot->credited_time}} @else <i class="fa fa-times" aria-hidden="true"></i> @endif </td>
                                        <td style="text-align: center">@if(!empty($participant->pivot->grade)){{$participant->pivot->grade}} @else <i class="fa fa-times" aria-hidden="true"></i> @endif </td>
                                        @if(!empty($participant->pivot->report_link))
                                            <td><a target="_blank" href="{{$participant->pivot->report_link}}" title="{{$participant->pivot->report_link}}">Link</a></td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$participant->pivot->created_at}}</td>
                                    @endif
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tasks" role="tabpanel">
                @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                    <a href="{{route('task_new_form', [$event])}}" class="btn btn-sm btn-primary">{{trans('messages.add_new_task')}}</a>
                @endif
                <table id="tasks_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                                <th>{{trans('messages.action')}}</th>
                            @endif
                            <th>{{trans('messages.title')}}</th>
                            <th>{{trans('messages.students')}}</th>
                            <th>{{trans('messages.edu_level')}}</th>
                            <th>Start</th>
                            <th>{{trans('messages.end')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                {{--todo mark as done for students?--}}
                                @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                                    <td>
                                        <a title="{{trans('assign_student')}}" href="{{route('task_assign', ['task' => $task])}}" class="icon-table"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <a title="{{trans('edit_task')}}" href="{{route('task_edit', ['event' => $event, 'task' => $task])}}" class="icon-table"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </td>
                                @endif
                                <td><a href="{{route('task_details', [$task->id])}}" style="text-decoration: none; color: #4f5f6f">{{$task->title}}</a></td>
                                <td>{{$task->assignees()->count()}} / {{$task->max_users}}</td>
                                <td>{{$task->educational_level}}</td>
                                <td>{{$task->start_date}}</td>
                                <td>{{$task->end_date}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    $('#schedule').click(function (e) {
                        e.preventDefault();
                        window.location.href = $(this).attr('href')
                    });
                    $('#participants_table').DataTable({
                        "language": {
                            "emptyTable": '{{trans('messages.no_student_found')}}',
                            @if(App::getLocale() == 'en')
                            "url": "{{URL::asset('assets/dataTables/en.json')}}"
                            @else
                            "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                            @endif
                        },
                        @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                        order: [[1, 'desc']],
                        @endif
                        columns: [
                            @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                            //Action
                            {
                                orderable: false,
                                width: "5%"
                            },
                            @endif
                            @if(Laratrust::hasRole(['docent']))
                            //Status
                            null,
                            @endif
                            //Name
                            null,
                            //Groups(s)
                            null,
                            @if(Laratrust::hasRole(['docent', 'admin']))
                            //Credited time
                            {
                                render: function(data){
                                    return ($.isNumeric(data)) ? data + ' uur' : data;
                                }
                            },
                            //grade
                            null,
                            //Event report link
                            null,
                            //signed up at
                            {
                                render: function(data){
                                    var date = moment(data);
                                    return date.format('DD/MM/YYYY kk:ss');
                                }
                            }
                            @endif
                        ]
                    });
                    $('#tasks_table').DataTable({
                        "language": {
                            "emptyTable": '{{trans('messages.no_tasks_found')}}',
                            @if(App::getLocale() == 'en')
                            "url": "{{URL::asset('assets/dataTables/en.json')}}"
                            @else
                            "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                            @endif
                        },
                        order: [[1, 'desc']],
                        columns: [
                            @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                            //Action
                            {
                                orderable: false,
                                width: "5%"
                            },
                            @endif
                            //title
                            null,
                            //students
                            null,
                            //level
                            null,
                            //start
                            {
                                render: function(data){
                                    if(data){
                                        var date = moment(data);

                                        return date.format('DD/MM/YYYY kk:ss');
                                    }else{
                                        return 'N/A';
                                    }
                                }
                            },
                            //end
                            {
                                render: function(data){
                                    if(data){
                                        var date = moment(data);
                                        return date.format('DD/MM/YYYY kk:ss');
                                    }else{
                                        return 'N/A';
                                    }
                                }
                            }
                        ]
                    });
                    //For clicking on the tabs
                    $('.nav-item a').click(function (e) {
                        e.preventDefault();
                        $(this).tab('show')
                    });
                    //Fill in grade modal with specific student data
                    $('#grade_modal').on('show.bs.modal', function(e) {
                        var userId  = e.relatedTarget.id,
                            eventId = {{$event->id}};
                        $.ajax({
                            type: 'GET',
                            url: '/event/getgradeinfo',
                            data: {'user': userId, 'event': eventId },
                            success: function(data) {
                                $('#grade_form').attr('action', data.route);
                                $('#grade').val(data.grade);
                                $('#time').val(data.credited_time);
                                if(data.report_link != null){
                                    $('#report').html('<a href="'+data.report_link+'" target="_blank">'+data.report_link+'</a>');
                                }else{
                                    $('#report').html('');
                                }
                                $('#grade_form').append('<input type="hidden" name="student" value="'+userId+'" >');
                                $('#grade_form').append('<input type="hidden" name="event" value="'+eventId+'" >');
                                var total = 0;
                                $('.grade_task_table').empty();
                                $.each(data.tasks, function(index, value){
                                    total += parseFloat(value[1]);
                                    $('.grade_task_table').append('<tr><td>'+value[0]+'</td><td>'+value[1]+' uur</td></tr>');
                                });
                                $('.grade_task_table').append('<tr><td><b class="pull-right">{{trans('messages.total')}}:</b></td><td>'+total+' uur</td></tr>');
                            }
                        });
                    });
                    //Fill in notes modal
                    $('#notes_modal').on('show.bs.modal', function(e) {
                        var userId  = e.relatedTarget.id,
                            eventId = {{$event->id}};
                        $.ajax({
                            type: 'GET',
                            url: '/event/getstudentnote',
                            data: {'user': userId, 'event': eventId },
                            success: function(data) {
                                $('#note_form').append('<input type="hidden" name="student" value="'+userId+'" >');
                                $('#note_form').append('<input type="hidden" name="event" value="'+eventId+'" >');
                                $('#note_form').attr('action', data.route);
                                $('#notes').val(data.notes);
                            }
                        });
                    });
                });
            </script>
        @endpush
    </div>
    @if(Laratrust::hasRole(['docent']))
        <div class="modal fade" id="grade_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">{{trans('messages.grade')}} student</h4>
                    </div>
                    <form action="" method="POST" id="grade_form">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <p>{{trans('messages.grade_modal_note')}}</p>
                            <div class="form-group row">
                                <label for="report" class="col-xs-2">{{trans('messages.report')}}:</label>
                                <div class="col-xs-10" id="report">
                                    {{--report link if there is one--}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="grade" class="col-xs-2">{{trans('messages.grade')}}:</label>
                                <div class="col-xs-10">
                                    <input type="text" name="grade" id="grade" class="form-control" placeholder="1 - 10 or O/M/V/G">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <p><b>{{trans('messages.student_assigned_tasks')}}</b></p>
                                </div>
                                <div class="col-xs-12">
                                    <table class="table table-bordered grade_task_table" cellspacing="0" width="100%">
                                        {{--Filled in by jquery -> All tasks the student is assigned to for this event (title+credit)--}}
                                    </table>
                                    <label for="time" class="col-xs-3">{{trans('messages.credited_time')}}:</label>
                                    <div class="col-xs-9">
                                        <input type="text" name="time" id="time" class="form-control" placeholder="{{trans('messages.time_placeholder')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('messages.close')}}</button>
                            <button type="submit" class="btn btn-primary">{{trans('messages.save_changes')}}</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
    @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
        <div class="modal fade" id="notes_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Student {{trans('messages.notes')}}</h4>
                    </div>
                    <form action="" method="POST" id="note_form">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <p>{{trans('messages.notes_modal_desc1')}} <br> {{trans('messages.notes_modal_desc2')}}</p>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <textarea name="notes" class="form-control" id="notes" rows="7" style="resize: none"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('messages.close')}}</button>
                            <button type="submit" class="btn btn-primary">{{trans('messages.save_changes')}}</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
    @if(Laratrust::hasRole(['leerling']))
        <div class="modal fade" id="report_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">{{trans('messages.event_report')}}</h4>
                    </div>
                    <form action="{{route('event_reportlink', [$event->id])}}" method="POST">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <p>{{trans('messages.report_modal_desc')}}</p>
                            <p>
                                <div class="form-group row">
                                    <label for="report_link" class="col-xs-1 col-form-label">Url:</label>
                                    <div class="col-xs-11">
                                        <input class="form-control" type="url" name="report_link" id="report_link"
                                           @if(Auth::user()->events->contains($event->id)) value="{{Auth::user()->events()->find($event->id)->pivot->report_link}}" @endif
                                        >
                                    </div>
                                </div>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('messages.close')}}</button>
                            <button type="submit" class="btn btn-primary">{{trans('messages.save_changes')}}</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="modal fade" id="terms_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">{{trans('messages.t_and_c')}}</h4>
                    </div>
                    <form action="{{route('event_signup')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <p>@if(!empty($event->terms)){!!nl2br(e($event->terms))!!}@else *{{trans('messages.no_terms_found')}}* @endif</p>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <input type="checkbox" name="terms" value="1" required> {{trans('messages.accept_terms')}}
                                </div>
                                <input type="hidden" name="event" value="{{$event->id}}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('messages.close')}}</button>
                            <button type="submit" class="btn btn-primary">{{trans('messages.sign_me_up')}}</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@endsection