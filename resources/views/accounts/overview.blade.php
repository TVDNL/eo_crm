@extends('layouts.master')
@section('title', 'Accounts overview')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>{{trans('messages.active')}}</th>
            <th>{{trans('messages.name')}}</th>
            <th>Email</th>
            <th>{{trans('messages.role')}}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{!! $user->getStatusWithToggle() !!}</td>
                    <td><a href="{{route('profile', [$user->id])}}" style="text-decoration: none; color: #4f5f6f">{{$user->getFullName()}}</a>
                    <td>{{$user->email}}</td>
                    <td>{{$user->roles->first()->display_name}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @push('scripts')
        <script>
            $(document).ready(function() {
                //initialize the datatable
                $('#table').DataTable({
                    "language": {
                        @if(App::getLocale() == 'en')
                        "url": "{{URL::asset('assets/dataTables/en.json')}}"
                        @else
                        "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                        @endif
                    },
                    order: [[1, 'desc']],
                    columns: [
                        //Active
                        {
                          render: function(data){
                              return data;
                          }
                        },
                        //Name
                        null,
                        //Email
                        null,
                        //role
                        null
                    ]
                });
            } );
        </script>
    @endpush
@endsection