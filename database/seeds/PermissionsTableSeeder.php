<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name'          =>  'view-profile',
                'display_name'  =>  'View Profile',
                'description'   =>  'User may view profiles'
            ],
            [
                'name'          =>  'edit-own-profile',
                'display_name'  =>  'Edit own profile',
                'description'   =>  'User may edit his own profile'
            ]
        ];

        $admin = Role::where('name', '=', 'admin')->first();

        foreach($permissions as $permission){
            $perm = Permission::create($permission);

            //Attach all permissions to the admin role
            $admin->attachPermission($perm);
        }
    }
}
