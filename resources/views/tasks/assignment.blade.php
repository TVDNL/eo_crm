@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card card-block ">
                    <div class="title-block">
                        <h3 class="title">
                            {{$title}}
                        </h3>
                    </div>
                    <hr>
                    @include('errors.errors')
                    <form action="{{route('task_add_students',[$task])}}" method="POST">
                        {{ csrf_field() }}
                        <div id="listbox">
                            <select multiple="multiple" name="selected_students[]">
                                @foreach($people as $person)
                                    @if($person->hasRole('leerling'))
                                        <option value="{{$person->id}}" @if($task->assignees->contains($person->id)) selected @endif>
                                            {{$person->getFullName()}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        $('#listbox').bootstrapDualListbox({
                                            moveOnSelect: false,
                                            selectedListLabel: '{{trans('messages.selected_people')}}',
                                            nonSelectedListLabel: '{{trans('messages.available_people')}}',
                                            selectorMinimalHeight: 175
                                        });
//                                        todo redo this (preventing more than the max students be selected)
                                        {{--$('#listbox').on('change',function(){--}}
                                            {{--var size=$('#listbox').find(":selected").length;--}}
                                            {{--if(size>{{$task->max_users}}){--}}
                                                {{--$('#listbox').find(":selected").each(function(ind,sel){--}}
                                                    {{--//todo doesnt work great if max = 1--}}
                                                    {{--if(ind>={{$task->max_users}})--}}
                                                        {{--$(this).prop("selected", false)--}}
                                                {{--});--}}
                                                {{--$('#listbox').bootstrapDualListbox('refresh', true);--}}
                                            {{--}--}}
                                        {{--})--}}
                                    });
                                </script>
                            @endpush
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10"><p><b>Note:</b> {{trans('messages.task_note1')}} <b>{{$task->max_users}}</b> {{trans('messages.task_note2')}}</p></div>
                            <div class="col-sm-offset-2 col-sm-10"> <button type="submit" class="btn btn-success">{{trans('messages.add_student_s')}}</button> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection