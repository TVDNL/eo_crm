@extends('layouts.master')
@section('title', '404')
@section('content')
    <article class="content error-404-page">
        <section class="section">
            <div class="error-card">
                <div class="error-title-block">
                    <h1 class="error-title">404</h1>
                    <h2 class="error-sub-title">
                        {{trans('messages.sorry_not_found')}}
                    </h2>
                </div>
                <div class="error-container visible">
                    <br>
                    <a class="btn btn-primary" href="{{route('home')}}"><i class="fa fa-angle-left"></i>{{trans('messages.back_to_homepage')}}}</a>
                </div>
            </div>
        </section>
    </article>
@endsection