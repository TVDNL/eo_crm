<?php

use App\User;

class Helper
{
    /**
     * Returns the total awarded time credits from all events that the given user has participated in.
     *
     * @param User $user
     * @return int
     */
    public static function totalCredits(User $user)
    {
        $total_public = 0;
        $total_personal = 0;
        foreach($user->events()->get() as $event){
            if($event->type = 'public'){
                $total_public += floatval(str_replace(',','.',$event->participants()->find($user->id)->pivot->credited_time));
            }else{
                $total_personal += floatval(str_replace(',','.',$event->participants()->find($user->id)->pivot->credited_time));
            }
        }
        $total_public = str_replace('.',',',$total_public);
        $total_personal = str_replace('.',',',$total_personal);
        return trans('messages.public').': &nbsp;'.$total_public.' uur&nbsp;&nbsp;/&nbsp;&nbsp;'.trans('messages.personal').': &nbsp;'.$total_personal.' uur';
    }
}