@extends('layouts.master')
@section('title', 'Preview accounts')
@section('content')
    <div class="container" style="max-width: 100%">
        <div class="row">
            <div class="col-md-12">
                <div class="title-block">
                    <h3 class="title">
                        {{$title}}
                    </h3>
                </div>
                @include('errors.errors')
                <form action="{{route('account_form_send', ['automatic'])}}" method="POST">
                    {{ csrf_field() }}
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <td><input type="checkbox" name="select_all"  id="table-select-all"></td>
                                    <td>School ID</td>
                                    <td>{{trans('messages.name')}}</td>
                                    <td>{{trans('messages.prefix')}}</td>
                                    <td>{{trans('messages.lastname')}}</td>
                                    <td>E_mailadres</td>
                                    <td>{{trans('messages.gender')}}</td>
                                    <td>{{trans('messages.date_of_birth')}}</td>
                                    <td>{{trans('messages.address')}}</td>
                                    <td>{{trans('messages.zipcode')}}</td>
                                    <td>{{trans('messages.city')}}</td>
                                    <td>{{trans('messages.phonenumber')}}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $student)
                                    <tr>
                                        @foreach($student as $value)
                                            <td>
                                                {{$value}}
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="title-block">
                        <h6 class="title">
                            {{trans('messages.option_group')}}
                        </h6>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 form-control-label">
                            {{trans('messages.name')}}
                        </label>
                        <div class="col-sm-4">
                            <input name="name" type="text" class="form-control" id="name" value="" maxlength="255">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 form-control-label">
                            {{trans('messages.description')}}
                        </label>
                        <div class="col-sm-4">
                            <input name='description' type="text" class="form-control" id="description" value="" maxlength="2000">
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button class="btn btn-success" id="sendForm">
                                {{trans('messages.create_accs_email')}}
                            </button>
                        </div>
                    </div>
                </form>
                @push('scripts')
                    <script>
                        $(document).ready(function() {
                            $('#sendForm').on('click', function(e){
                                if($('tbody :checkbox:checked').length > 0){
                                    $('#sendForm').submit();
                                }else{
                                    e.preventDefault();
                                    alert('{{trans('messages.select_1_student')}}');
                                    return  false;
                                }
                            });

                            var table = $('#table');

                            //initialize the datatable
                            table.DataTable({
                                "language": {
                                    @if(App::getLocale() == 'en')
                                    "url": "{{URL::asset('assets/dataTables/en.json')}}"
                                    @else
                                    "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                                    @endif
                                },
                                order: [[4, 'asc']],
                                columns: [
                                    {
                                        //Select boxes

                                        orderable: false,
                                        searchable: false,
                                        render: function (data, type, full, meta){
                                            if(data == 'acc_exists') return '<i class="fa fa-check-circle-o" aria-hidden="true" title="{{trans('messages.user_exist')}}"></i>';
                                            return '<input type="checkbox" name="users[]" value="' + full[1] + '">';
                                        }
                                    },
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    {
                                        // Date of birth

                                        render: function(data){
                                            var date = moment(data);
                                            return date.format('DD/MM/YYYY');
                                        }
                                    },
                                    null,
                                    null,
                                    null,
                                    null
                                ]
                            });

                            // Handle click on "Select all" control
                            $('#table-select-all').on('click', function(){
                                $(':checkbox').prop('checked',this.checked);
                            });
                        } );
                    </script>
                @endpush
            </div>
        </div>
    </div>
@endsection