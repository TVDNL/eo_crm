@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="card card-block sameheight-item">
                    <div class="title-block">
                        <h3 class="title">
                            {{$title}}
                        </h3>
                    </div>
                    <hr>
                    @if(session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('errors.errors')
                    <form action="{{$route}}" method="POST" id="task_form">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="title" class="col-sm-3 form-control-label">{{trans('messages.title')}}:*</label>
                            <div class="col-sm-9">
                                <input name='title' type="text" class="form-control" id="title" maxlength="255" value="{{$task->title}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 form-control-label">{{trans('messages.description')}}:</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control" id="description" rows="7" style="resize: none" maxlength="2000">{{$task->description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="maxstudents" class="col-sm-3 form-control-label">Max nr. of {{trans('messages.students')}}:</label>
                            <div class="col-sm-9">
                                <input name='maxstudents' type="number" min="1" class="form-control" id="maxstudents" value="{{$task->max_users}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="credit" class="col-sm-3 form-control-label">{{trans('messages.credit')}}:*</label>
                            <div class="col-sm-9">
                                <input id="credit" class="form-control" name="credit" type="number" placeholder="0,5 = 30min" step="0.5" value="{{$task->credit}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dates" class="col-sm-3 form-control-label">Start & {{trans('messages.end')}}*</label>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <div class='input-group date' id='startdate'>
                                        <input type='text' class="form-control" name="startdate" value="{{date_format(date_create($task->start_date), 'd/m/Y  H:i')}}" required>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <div class='input-group date' id='enddate'>
                                        <input type='text' class="form-control" name="enddate" value="{{date_format(date_create($task->end_date), 'd/m/Y  H:i')}}" required/>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-3 form-control-label">{{trans('messages.edu_level')}}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name='level[]' value="1" @if(in_array('1', explode(', ', $task->educational_level))) checked @endif > 1
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name='level[]' value="2" @if(in_array('2', explode(', ', $task->educational_level))) checked @endif > 2
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name='level[]' value="3" @if(in_array('3', explode(', ', $task->educational_level))) checked @endif > 3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button id="standard_submit" class="btn btn-success">{{$button}}</button>
                                @if(!empty($extraButton))
                                    <button id="extra_submit" class="btn btn-success">{{$extraButton}}</button>
                                @else
                                    <button id="delete_task" class="btn btn-danger">{{trans('messages.delete_task')}}</button>
                                @endif
                            </div>
                        </div>
                    </form>
                    @push('scripts')
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('#delete_task').on('click', function(e){
                                    e.preventDefault();
                                    var r = confirm('{{trans('messages.task_confirm_delete')}}');
                                    if (r == true) {
                                        window.location.href = '{{route('task_delete', [$task])}}';
                                    }
                                });
                                $('#standard_submit').on('click', function(){
                                    $('#task_form').submit();
                                });
                                $('#extra_submit').on('click', function(e){
                                    e.preventDefault();
                                    $('#task_form').append('<input type="hidden" name="duplicate" value="true">');
                                    $('#task_form').submit();
                                });
                                $(function () {
                                    $('#startdate').datetimepicker({
                                        minDate: '{{$event->startDate}}',
                                        maxDate: '{{$event->endDate}}',
                                        format: 'D/MM/YYYY  HH:mm',
                                        icons: {
                                            time:"fa fa-clock-o",
                                            date:"fa fa-calendar",
                                            up:"fa fa-chevron-up",
                                            down:"fa fa-chevron-down",
                                            previous:"fa fa-chevron-left",
                                            next:"fa fa-chevron-right",
                                            today:"fa fa-crosshairs",
                                            clear:"fa fa-trash",
                                            close:"fa fa-times"
                                        }
                                    });
                                    $('#enddate').datetimepicker({
                                        useCurrent: false, //Important! See issue #1075
                                        minDate: '{{$event->startDate}}',
                                        maxDate: '{{$event->endDate}}',
                                        format: 'D/MM/YYYY  HH:mm',
                                        icons: {
                                            time:"fa fa-clock-o",
                                            date:"fa fa-calendar",
                                            up:"fa fa-chevron-up",
                                            down:"fa fa-chevron-down",
                                            previous:"fa fa-chevron-left",
                                            next:"fa fa-chevron-right",
                                            today:"fa fa-crosshairs",
                                            clear:"fa fa-trash",
                                            close:"fa fa-times"
                                        }
                                    });
                                    $("#startdate").on("dp.change", function (e) {
                                        $('#enddate').data("DateTimePicker").minDate(e.date);
                                    });
                                    $("#enddate").on("dp.change", function (e) {
                                        $('#startdate').data("DateTimePicker").maxDate(e.date);
                                    });
                                });
                            });
                        </script>
                    @endpush
                </div>
            </div>
        </div>
    </div>
@endsection