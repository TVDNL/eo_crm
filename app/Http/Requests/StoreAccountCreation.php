<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAccountCreation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->hasRole(['docent'])) ? true : abort(403);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->type == 'manual'){
            return [
                'id'          => 'max:9999999|unique:users,school_id',
                'firstname'   => 'required|max:255',
                'prefix'      => 'max:255',
                'lastname'    => 'required|max:255',
                'email'       => 'required|email|unique:users,email',
                'dateofbirth' => 'date',
                'address'     => 'max:255',
                'zipcode'     => 'max:255',
                'city'        => 'max:255',
                'phonenumber' => 'max:255',
                'allergies'   => 'max:255',
                'size'        => 'max:10',
                'calamity'    => 'max:255',
                'hygiene'     => 'required|boolean',
                'bhv'         => 'required|boolean',
                'notes'       => 'max:2000',
                'role'        => 'required|in:leerling,docent,opdrachtgever'
            ];
        }else{
            return [
                'name'        => 'max:255',
                'description' => 'max:2000',
                'users'       => 'required|array|filled'
            ];
        }
    }
}