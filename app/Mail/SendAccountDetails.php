<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class SendAccountDetails extends Mailable
{
    use Queueable, SerializesModels;

    public  $user;
    public  $pw;
    public  $role;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user, $pw)
    {
        $this->user = $user;
        $this->pw = $pw;
        $this->role = $user->roles()->first()->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.accountdetails')
                    ->from(Auth::user()->email)
                    ->subject(trans('messages.mail_subject'));
    }
}