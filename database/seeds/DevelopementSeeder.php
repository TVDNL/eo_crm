<?php

use Illuminate\Database\Seeder;

class DevelopementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'email'     => 'thomas@test.com',
                'password'  => Hash::make('thomas'),
                'firstname' => 'Thomas',
                'lastname_prefix'  => 'van',
                'lastname'  => 'Dellen',
                'school_id' => '6451097',
                'dateofbirth' => '1991-09-17 00:00:00',
                'address' => 'Valklaan 6',
                'zipcode' => '9648 AK',
                'city'    => 'Wildervank',
                'phonenumber'    => '0654851258',
                'size'    => 'L',
                'allergies'    => 'Spruitjes',
            ],
            [
                'email'     => 'leerling@leerling.com',
                'password'  => Hash::make('leerling'),
                'firstname' => 'Pietje',
                'lastname_prefix'  => 'de',
                'lastname'  => 'Tester',
                'school_id' => '2457096',
                'dateofbirth' => '1996-04-04 00:00:00',
                'address' => 'Boslaan 77',
                'zipcode' => '5687 PI',
                'city'    => 'Oss',
                'phonenumber'    => '0654239517',
                'size'    => 'XL',
                'allergies'    => 'Katten',
                'notes' => 'Komt regelmatig laat in de les -> moet nog worden aangesproken'
            ],
            [
                'email'     => 'docent@docent.com',
                'password'  => Hash::make('docent'),
                'firstname' => 'Reygan',
                'lastname_prefix'  => '',
                'lastname'  => 'Leito'
            ],
            [
                'email'     => 'opdrachtgever@opdrachtgever.com',
                'password'  => Hash::make('opdrachtgever'),
                'firstname' => 'Johan',
                'lastname_prefix'  => 'van',
                'lastname'  => 'Berenburg'
            ]

        ];

        foreach ($users as $user){
            $person = \App\User::firstOrCreate($user);
            switch ($person->email){
                case 'thomas@test.com' :
                    $person->attachRole(\App\Role::where('name', '=', 'leerling')->first());
                    break;
                case 'leerling@leerling.com' :
                    $person->attachRole(\App\Role::where('name', '=', 'leerling')->first());
                    break;
                case 'docent@docent.com' :
                    $person->attachRole(\App\Role::where('name', '=', 'docent')->first());
                    break;
                case 'opdrachtgever@opdrachtgever.com' :
                    $person->attachRole(\App\Role::where('name', '=', 'opdrachtgever')->first());
                    break;
            }
        }

        $groups = [
            [
                'name' => 'AMO3',
                'description' => '3e jaars AMo opleiding.'
            ]
        ];

        foreach ($groups as $group){
            $group = \App\Group::create($group);
            $group->members()->attach(['1','2','3']);
        }

        $events = [
            [
                'type' => 'public',
                'title' => 'Rommelmarkt',
                'description' => 'Verkoop al je oude spullen!',
                'educational_level' => ['3'],
                'total_hours' => '9',
                'min_users' => '8',
                'max_users' => '25',
                'startDate' => '2017-02-14 09:00:00',
                'endDate' => '2017-02-14 18:00:00',
                'created_by' => '3',
                'address' => 'Verzetsstrijderslaan 74',
                'zipcode' => '4569 KR',
                'city' => 'Groningen'
            ],
            [
                'type' => 'public',
                'title' => 'Open dag',
                'description' => 'Informeer toekomstige studenten',
                'educational_level' => ['2'],
                'total_hours' => '18',
                'min_users' => '1',
                'max_users' => '6',
                'startDate' => '2017-04-23 09:00:00',
                'endDate' => '2017-04-25 18:00:00',
                'created_by' => '3',
                'address' => 'Almastraat 154',
                'zipcode' => '4598 HT',
                'city' => 'Groningen'
            ]
        ];

        foreach ($events as $event){
            $event = \App\Event::create($event);
            if($event->id == 1){
                $event->participants()->attach('2',['status' => 'accepted']);
                $event->participants()->attach('3',['status' => 'accepted']);
                $event->participants()->attach('4',['status' => 'accepted']);
            }else{
                $event->participants()->attach('3',['status' => 'accepted']);
                $event->participants()->attach('4',['status' => 'accepted']);
            }
        }
    }
}