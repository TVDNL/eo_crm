<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if(Auth::user()->hasRole(['docent'])){
            $outstanding_reviews = [];
            foreach(Auth::user()->events()->get() as $event){
                $users = $event->participants()->whereHas('roles', function($q){
                    $q->where('name', '=', 'leerling');
                })->get();
                foreach ($users as $user){
                    if($user->pivot->status == 'accepted' && ($user->pivot->status == null || $user->pivot->credited_time == null)){
                        array_push($outstanding_reviews, ['user' => $user, 'event' => $event]);
                    }
                }
            }
        }else{
            $outstanding_reviews = null;
        }

        return view('home',[
            'outstanding_reviews' => $outstanding_reviews
        ]);
    }
}