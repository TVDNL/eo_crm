<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

//In master.blade the routes prefixes  (event/group/profile etc.) is used hardcoded for the menu hightlighting.

//Disable registering
Route::any('/register', 'HomeController@show');
//Route to switch languages
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::group(['middleware' => ['auth']], function () {
    //Change logout back to GET
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    //Show Homepage
    Route::get('/home', 'HomeController@show')->name('home');
    Route::get('/',     'HomeController@show');

    //Search bar
    Route::get('find', 'SearchController@find');

    //User profile page (Warning!: This route is used hardcoded for the links used in the searchbar results)
    Route::get('/profile/{user}', 'AccountController@viewProfile')->name('profile');

    //remove user from event
    Route::get('/event/removesignup/{event}', 'EventController@removeSignup')->name('event_remove_signup');
    // Show overview page of all events
    Route::get('/event/overview', 'EventController@overview')->name('event_overview');
    //Event details page (Warning!: This route is used hardcoded for the links used in the searchbar results)
    Route::get('/event/details/{event}', 'EventController@view')->name('event_details');
    //sign up for event
    Route::post('/event/signup', 'EventController@signup')->name('event_signup');
    //Save cloudlink to event report todo make sure student can only upload their own links
    Route::post('/event/reportlink/{event}', 'EventController@storeReportlink')->name('event_reportlink');

    // Show overview page of all groups
    Route::get('/group/overview', 'GroupController@overview')->name('group_overview');
    //Group details page (Warning!: This route is used hardcoded for the links used in the searchbar results)
    Route::get('/group/details/{group}', 'GroupController@view')->name('group_details');

    //Task details page
    Route::get('/task/details/{task}', 'TaskController@view')->name('task_details');
    //Go to schedule with all the event's tasks
    Route::get('/task/schedule/{event}', 'TaskController@schedule')->name('task_schedule');
    //Export the schedule to excel
    Route::get('/task/export/{event}', 'TaskController@export')->name('task_export_schedule');
    //Get task info for in the schedule modal
    Route::get('/task/gettaskmodalinfo', 'TaskController@tasksModal')->name('task_schedule_modal_info');

    // All docent only routes
    Route::group(['middleware' => ['role:docent']], function () {
        //Page with form to create new event
        Route::get('/event/create', 'EventController@form')->name('event_create');
        //Accept or decline event participant
        Route::get('/event/{event}/{user}/{status}', 'EventController@participantStatus')->name('event_participant_status');
        //Form page to manual add people to an event
        Route::get('/event/assignment/{event}', 'EventController@manualAssignment')->name('event_manual_assignment');
        //Add participant(s) to the event
        Route::post('/event/addparticipant/{event}', 'EventController@addParticipants')->name('event_add_participants');
        //Retrieve student information for the grade modal (Warning!: This route is used hardcoded for the ajax call in event details)
        Route::get('/event/getgradeinfo/', 'EventController@getGradeInfo')->name('event_grade_info');
        //Store the students grades
        Route::post('/event/storegrades', 'EventController@storeGrades')->name('event_store_grades');

        //Page with form to create new group
        Route::get('/group/create', 'GroupController@form')->name('group_create');
        //Page with form to edit group
        Route::get('/group/edit/{group}', 'GroupController@form')->name('group_edit');
        //Store the group in DB
        Route::post('/group/store/{group?}', 'GroupController@store')->name('group_form_send');

        // Show overview page of all users
        Route::get('/account/overview', 'AccountController@overview')->name('account_overview');
        //Store the new user(s) in DB
        Route::post('/account/store/{type}/{user?}', 'AccountController@store')->name('account_form_send');
        // Show the the account previews of the uploaded excel
        Route::post('/account/preview', 'AccountController@preview')->name('account_preview');
        //Updates users active status
        Route::get('/account/changestatus/{user}', 'AccountController@changeUserStatus')->name('account_change_status');
    });

    //All docent OR leerling routes
    Route::group(['middleware' => ['role:docent|leerling']], function () {
        //Page with form to create or edit account(s)
        Route::get('/account/form/{type}/{user?}', 'AccountController@form')->name('account_form');
        //Store the edited user
        Route::post('/account/edit/{user}', 'AccountController@edit')->name('account_form_send_edit');
    });

    //All docent OR opdrachtgever routes
    Route::group(['middleware' => ['role:docent|opdrachtgever']], function () {
        //Page with form to edit event
        Route::get('/event/edit/{event}', 'EventController@form')->name('event_edit');
        //Store the event in DB
        Route::post('/event/store/{event?}', 'EventController@store')->name('event_form_send');
        //Retrieve student information for the note modal (Warning!: This route is used hardcoded for the ajax call in event details)
        Route::get('/event/getstudentnote/', 'EventController@getStudentNote')->name('event_student_note');
        //Store the student note for the event
        Route::post('/event/storestudentnote', 'EventController@StoreStudentNote')->name('event_store_student_note');

        //Form to edit a task
        Route::get('/task/edit/{event}/{task}', 'TaskController@form')->name('task_edit');
        //Form to create a new task from a template
        Route::get('/task/create/duplicate/{task}', 'TaskController@duplicate')->name('task_new_duplicate');
        //Form to create a new task
        Route::get('/task/create/{event}', 'TaskController@form')->name('task_new_form');
        //Store task to DB
        Route::post('/task/store/{event}/{task?}', 'TaskController@store')->name('task_store');
        //Form to assign students to a task
        Route::get('/task/assign/{task}', 'TaskController@assignmentForm')->name('task_assign');
        //Store the student-task relations
        Route::post('/task/assignment/{task}', 'TaskController@assignment')->name('task_add_students');
        //Delete the task
        Route::get('/task/delete/{task}', 'TaskController@delete')->name('task_delete');
    });
});