@extends('layouts.master')
@section('title', 'Task details')
@section('content')
    @push('custom-css')
        <style>
            .details {margin-bottom: 66px}
        </style>
    @endpush
    <div class="container">
        <div class="title-block">
            <h3 class="title">
                {{trans('messages.task')}} details <span class="sparkline bar" data-type="bar"></span>
                @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
                    <a href="{{route('task_edit', ['event' => $task->event_id, 'task' => $task->id])}}" class="btn btn-sm btn-primary">{{trans('messages.edit_event')}}</a>
                @endif
            </h3>
        </div>
        <div class="details">
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.title')}}:
                </label>
                <div> {{$task->title}} </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.event')}}:
                </label>
                <a href="{{route('event_details', ['event' => \App\Event::find($task->event_id)->id])}}" style="text-decoration: none; color: #4f5f6f">{{\App\Event::find($task->event_id)->title}}</a>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.description')}}:
                </label>
                <div> {{$task->description}} </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.credit')}}:
                </label>
                <div> {{$task->credit}} </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.assignees')}}:
                </label>
                <div> {{$task->assignees()->count()}} / {{$task->max_users}} </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.edu_level')}}:
                </label>
                <div> {{$task->educational_level}} </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    {{trans('messages.date')}}:
                </label>
                <div>{{$task->start_date->format('d/m/Y  H:i')}}  -  {{$task->end_date->format('d/m/Y  H:i')}}</div>
            </div>
        </div>
        <div class="title-block">
            <h5 >
                {{trans('messages.assigned_students')}} <span class="sparkline bar" data-type="bar"></span>
            </h5>
        </div>
        @if(Laratrust::hasRole(['docent', 'opdrachtgever']))
            <a href="{{route('task_assign', ['task' => $task])}}" class="btn btn-sm btn-primary">{{trans('messages.assign_student_s')}}</a>
        @endif
        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{trans('messages.name')}}</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                @foreach($task->assignees()->get() as $user)
                    <tr>
                        <td><a href="{{route('profile', [$user->id])}}" style="text-decoration: none; color: #4f5f6f">{{$user->getFullName()}}</a></td>
                        <td>{{$user->email}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @push('scripts')
            <script>
                $(document).ready(function() {
                    //initialize the datatable
                    $('#table').DataTable({
                        "language": {
                            @if(App::getLocale() == 'en')
                            "url": "{{URL::asset('assets/dataTables/en.json')}}"
                            @else
                            "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                            @endif
                        },
                        columns: [
                            //name
                            null,
                            //email
                            null
                        ]
                    });
                });
            </script>
        @endpush
    </div>
@endsection