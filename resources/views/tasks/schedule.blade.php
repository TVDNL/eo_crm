@extends('layouts.master')
@section('title', 'Tasks schedule')
@section('content')
    @push('custom-css')
        <style>
            .table th{vertical-align: middle; white-space: nowrap;}
            #student_row td{border: solid 1px #000000}
            #total{border: solid 1px #000000}
            ::-webkit-scrollbar-thumb{
                background: #000000;
            }
            ::-webkit-scrollbar-thumb:window-inactive{
                background: #000000;
            }
        </style>
    @endpush

    <div class="title-block">
        <h3 class="title">
            {{ucfirst(trans('messages.schedule'))}} {{$event->title}} <span class="sparkline bar" data-type="bar"></span>
        </h3>
    </div>
    @include('tasks.table', ['event' => $event, 'header' => $header, 'students' => $students])
    <br>
    <a href="{{route('task_export_schedule', [$event])}}">{{trans('messages.export_excel')}}</a>
    @push('scripts')
        <script>
            $(document).ready(function() {
                //Fill in tasks modal
                $('#tasks_modal').on('show.bs.modal', function(e) {
                    var id  = e.relatedTarget.dataset.student;
                    var tasks = e.relatedTarget.dataset.tasks;
                    var modal_table = $('#task_rows');
                    modal_table.empty();
                    $('.modal-title').empty();
                    $.ajax({
                        type: 'GET',
                        url: '/task/gettaskmodalinfo',
                        data: {'tasks': tasks, 'student': id },
                        success: function(data) {
                            $('.modal-title').append('{{trans('messages.tasks')}} - '+data.name);
                            $.each(data.rows, function(key, value){
                                modal_table.append('<tr><td>'+value[0]+'</td><td>'+moment(value[1].date).format('DD/MM/YYYY kk:ss')+'</td><td>'+moment(value[2].date).format('DD/MM/YYYY kk:ss')+'</td></tr>');
                            });
                        }
                    });
                });
            });
        </script>
    @endpush
    <div class="modal fade" id="tasks_modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <table style="border-collapse: separate;border-spacing: 10px">
                        <thead>
                            <tr>
                                <th>{{trans('messages.title')}}</th>
                                <th>Start</th>
                                <th>{{trans('messages.end')}}</th>
                            </tr>
                        </thead>
                        <tbody id="task_rows">
                            {{--Insert tasks details--}}
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('messages.close')}}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection