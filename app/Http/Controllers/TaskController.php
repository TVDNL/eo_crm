<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\StoreTask;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class TaskController extends Controller
{
    public function view(Task $task)
    {
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!Event::find($task->event_id)->participants->contains(Auth::id())) abort(403);
        }
        if(Auth::user()->hasRole('leerling')){
            if(!$task->assignees->contains(Auth::id())) abort(403);
        }

        return view('tasks.details',[
            'task' => $task,
        ]);
    }

    public function form(Event $event, Task $task = null)
    {
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!$event->participants->contains(Auth::id())) abort(403);
        }

        if($task->id == null){
            $title = trans('messages.create_task_for').' '.$event->title;
            $route = route('task_store',[$event]);
            $button = trans('messages.create_task');
            $extraButton = trans('messages.create_task_duplicate');
        }else{
            $title = trans('messages.edit_task');
            $route = route('task_store',[$event, $task]);
            $button = trans('messages.save');
            $extraButton = '';
        }

        return view('tasks/form',[
            'title' => $title,
            'route' => $route,
            'button' => $button,
            'event' => $event,
            'task' => $task,
            'extraButton' => $extraButton
        ]);
    }

    public function duplicate(Task $task)
    {
        $event = Event::find($task->event_id);
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!$event->participants->contains(Auth::id())) abort(403);
        }

        $title = trans('create_task_for').' '.$event->title;
        $route = route('task_store',[$event]);
        $button = trans('messages.create_task');
        $extraButton = trans('messages.create_task_duplicate');

        return view('tasks/form',[
            'title' => $title,
            'route' => $route,
            'button' => $button,
            'event' => $event,
            'task' => $task,
            'extraButton' => $extraButton
        ]);
    }

    public function assignmentForm(Task $task)
    {
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!Event::find($task->event_id)->participants->contains(Auth::id())) abort(403);
        }

        $selection = User::whereHas('events', function($query) use ($task){
            $query->where('event_id', '=', $task->event_id)->where('event_user.status', '=', 'accepted');
        })->get();

        return view('tasks/assignment',[
            'task' => $task,
            'title' => trans('messages.assign_student').' '.$task->title,
            'people' => $selection
        ]);
    }

    public function assignment(Request $request, Task $task)
    {
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!Event::find($task->event_id)->participants->contains(Auth::id())) abort(403);
        }

        $this->validate($request, [
            'selected_students' => 'array|max:'.$task->max_users
        ]);

        $students = (is_array($request->selected_students)) ? $request->selected_students : [];
        $task->assignees()->sync($students);

        return redirect()->route('event_details',[$task->event_id])->with('status', trans('messages.assignees_suc_updated'));
    }

    public function store(StoreTask $request, Event $event, Task $task = null)
    {
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!$event->participants->contains(Auth::id())) abort(403);
        }
        if($task->id == null){
            $task = new Task();
            if(!isset($request->duplicate)){
                $redirect = redirect()->route('event_details',[$event])->with('status', trans('messages.task_suc_created'));
            };
        }else{
            $redirect = redirect()->route('event_details',[$event])->with('status', trans('messages.task_suc_edited'));
        }

        $task->title             = $request->title;
        $task->description       = $request->description;
        $task->max_users         = $request->maxstudents;
        $task->credit            = $request->credit;
        $task->start_date        = (!empty($request->startdate)) ? Carbon::createFromFormat('d/m/Y H:i', $request->startdate) : null;
        $task->end_date          = (!empty($request->enddate)) ? Carbon::createFromFormat('d/m/Y H:i', $request->enddate) : null;
        $task->educational_level = $request->level;
        $task->event_id          = $event->id;
        $task->save();

        if(isset($request->duplicate)){
            $redirect = redirect()->route('task_new_duplicate',[$task])->with('status', trans('messages.task_suc_created'));
        }

        return $redirect;
    }

    public function delete(Task $task)
    {
        if(Auth::user()->hasRole('opdrachtgever')){
            if(!Event::find($task->event_id)->participants->contains(Auth::id())) abort(403);
        }

        $event_id = $task->event_id;
        $task->assignees()->detach();
        $task->delete();
        return redirect()->route('event_details',[$event_id])->with('status', trans('messages.task_suc_deleted'));
    }

    public function dayOfWeek($day)
    {
        switch($day){
            case 0:
                return trans('messages.sunday');
                break;
            case 1:
                return trans('messages.monday');
                break;
            case 2:
                return trans('messages.tuesday');
                break;
            case 3:
                return trans('messages.wednesday');
                break;
            case 4:
                return trans('messages.thursday');
                break;
            case 5:
                return trans('messages.friday');
                break;
            case 6:
                return trans('messages.saturday');
                break;
            default:
                return 'Unknown';
                break;
        }
    }

    public function getScheduleHeader(Event $event, $dates_only = false)
    {
        //Get all the days that the event runs for the schedule header
        $header = [self::dayOfWeek($event->startDate->dayOfWeek).' '.$event->startDate->day.'/'.$event->startDate->month];
        $dates = [$event->startDate];
        $addition = 1;
        $startdate = $event->startDate->day;
        for($i=$startdate; $i<$event->endDate->day; $i++){
            if($event->startDate->addDay()->day <=  $event->endDate->day){
                array_push($dates, $event->startDate->addDay($addition));
                array_push($header, self::dayOfWeek($event->startDate->addDay($addition)->dayOfWeek).' '.$event->startDate->addDay($addition)->day.'/'.$event->startDate->addDay($addition)->month);
            }
            $addition++;
        }

        return ($dates_only) ? $dates : $header;
    }

    public function getScheduleData(Event $event, $day_totals = null)
    {
        $students = [];
        $dates = self::getScheduleHeader($event, true);
        $date_totals = [];
        foreach($event->participants()->whereHas('roles', function($q){$q->where('name', 'leerling');})->get() as $student){
            if($student->pivot->status == 'accepted') {
                $tasks = [];
                foreach ($dates as $date) {
                    $day = ['count' => 0];
                    $ids = [];
                    foreach ($student->tasks()->where('event_id', $event->id)->orderBy('start_date')->get() as $task) {
                        if (($date->day == $task->start_date->day) && ($date->month == $task->start_date->month) && ($date->year == $task->start_date->year)) {
                            $day['count']++;
                            array_push($ids, $task->id);
                            if (array_key_exists($date->toW3cString(), $date_totals)) {
                                $date_totals[$date->toW3cString()]++;
                            } else {
                                $date_totals[$date->toW3cString()] = 1;
                            }
                        }
                        if (!array_key_exists($date->toW3cString(), $date_totals)) {
                            $date_totals[$date->toW3cString()] = 0;
                        }
                    }
                    $day['ids'] = implode(',', $ids);
                    array_push($tasks, $day);
                }
                array_push($students, ['model' => $student, 'tasks' => $tasks]);
            }
        }

        return ($day_totals) ? $date_totals : $students;
    }

    public function schedule(Event $event)
    {
        return view('tasks.schedule',[
            'event' => $event,
            'header' => self::getScheduleHeader($event),
            'students' => self::getScheduleData($event),
            'day_totals' => self::getScheduleData($event, true)
        ]);
    }

    public function export(Event $event)
    {
        return Excel::create(trans('messages.schedule').'_'.strtolower($event->title), function($excel) use ($event){
            $excel->setTitle(trans('messages.schedule').'_'.$event->title)
                  ->setCreator(Auth::user()->getFullName())
                  ->setCompany('Noorderpoort')
            ;
            $excel->sheet('schedule', function($sheet) use ($event) {
                $sheet->loadView('tasks.table', ['event' => $event, 'header' => self::getScheduleHeader($event), 'students' => self::getScheduleData($event), 'day_totals' => self::getScheduleData($event, true)]);
            });
        })->export('xls');
    }

    public function tasksModal(Request $request)
    {
        $tasks = explode(',',$request->tasks);
        $student = User::find($request->student);
        $task_details = [];
        foreach ($tasks as $task){
            $model = Task::find($task);
            array_push($task_details, [$model->title,$model->start_date,$model->end_date]);
        }

        return ['name' => $student->getFullName(), 'rows' => $task_details];
    }
}