<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * todo Add roles descriptions
     *
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'          =>  'admin',
                'display_name'  =>  'Admin',
                'description'   =>  'TO be filled'
            ],
            [
                'name'          =>  'leerling',
                'display_name'  =>  'Leerling',
                'description'   =>  'TO be filled'
            ],
            [
                'name'          =>  'docent',
                'display_name'  =>  'Docent',
                'description'   =>  'TO be filled'
            ],
            [
                'name'          =>  'opdrachtgever',
                'display_name'  =>  'Opdrachtgever',
                'description'   =>  'TO be filled'
            ]
        ];

        foreach($roles as $role){
            Role::create($role);
        }
    }
}

