@extends('layouts.master')
@section('title', '403')
@section('content')
    <article class="content error-404-page">
        <section class="section">
            <div class="error-card">
                <div class="error-title-block">
                    <h1 class="error-title">403</h1>
                    <h2 class="error-sub-title">
                        {{trans('messages.access_denied')}}
                    </h2>
                </div>
                <div class="error-container visible">
                    <a class="btn btn-primary" href="{{route('home')}}"><i class="fa fa-angle-left"></i>{{trans('messages.back_to_homepage')}}</a>
                </div>
            </div>
        </section>
    </article>
@endsection