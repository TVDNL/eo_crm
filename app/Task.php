<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Task extends Model
{
    use SoftDeletes;
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'description' => 8,
            'educational_level' => 1
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hours', 'description', 'end_date', 'start_date', 'educational_level', 'created_by', 'updated_by'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['end_date', 'start_date', 'deleted_at'];

    public function setEducationalLevelAttribute($value)
    {
        $this->attributes['educational_level'] = (is_array($value)) ? implode(', ', $value) : null;
    }

    /**
     * Event the task is part from
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * Users assigned to this task
     */
    public function assignees()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}