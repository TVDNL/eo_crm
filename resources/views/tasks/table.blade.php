@push('custom-css')
    <style>
        td{text-align: center}
    </style>
@endpush
<div class="table-responsive">
    <table class="table table-striped">
        <tr style="background-color: #000; color: #ffffff ">
            <th>{{trans('messages.name')}}</th>
            <th>{{trans('messages.lastname')}}</th>
            <th>{{trans('messages.phonenumber')}}</th>
            <th>Emailadres</th>
            @foreach($header as $date)
                <th>{{$date}}</th>
            @endforeach
            <th>{{trans('messages.size')}}</th>
            <th>{{trans('messages.allergies')}}</th>
            <th>{{trans('messages.emergency_nr')}}</th>
        </tr>
        @forelse($students as $student)
            <tr id="student_row">
                <td>{{$student['model']->firstname}}</td>
                <td>@if(!empty($student['model']->lastname_prefix)) {{$student['model']->lastname_prefix.' '.$student['model']->lastname}} @else {{$student['model']->lastname}} @endif</td>
                <td>{{$student['model']->phonenumber}}</td>
                <td>{{$student['model']->email}}</td>
                @foreach($student['tasks'] as $day)
                    <td
                        @if($day['count'] == 0) style="background-color: #ff2f24" @endif>
                        @if($day['count'] != 0)
                            <a href="#tasks_modal"
                               data-tasks="{{$day['ids']}}"
                               data-student="{{$student['model']->id}}"
                               data-toggle="modal"
                               id="">
                                {{$day['count']}}
                            </a>
                        @else
                            {{$day['count']}}
                        @endif
                    </td>
                @endforeach
                <td>{{$student['model']->size}}</td>
                <td>{{$student['model']->allergies}}</td>
                <td>{{$student['model']->calamaity}}</td>
            </tr>
        @empty
            <tr><td>No students found for this event.</td></tr>
        @endforelse
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="pull-right">{{trans('messages.total')}}:</td>
            @foreach($day_totals as $total)
                <td id="total">{{$total}}</td>
            @endforeach
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>