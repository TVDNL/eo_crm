<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Maatwebsite\Excel\Facades\Excel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Custom validation rule for allowed time credit input
        Validator::extend('time_credit', function ($attribute, $value, $parameters, $validator) {
            $value = str_replace(',','.', $value);
            if(is_numeric($value) && $value >= 0.5){
                if((strlen(substr(strrchr($value, "."), 1)) >= 1)){
                    return (substr(strrchr($value, "."), 1) == 5) ? true : false;
                }
                return true;
            }

            return false;
        });

        //Custom validation rule for standard dutch grading
        Validator::extend('grade', function ($attribute, $value, $parameters, $validator) {
            $value = str_replace(',','.', $value);
            if(is_numeric($value) &&
               (strlen(substr(strrchr($value, "."), 1)) <= 1) &&
                $value >= 1 &&
                $value <= 10
              ){
                return true;
            }
            $non_numerical_grades = ['o','O','m','M','v','V','g','G'];
            if(in_array($value, $non_numerical_grades)){
                return true;
            }

            return false;
        });

        //Custom validation rule for allowed excel files for automatic student creation
        Validator::extend('student_excel', function ($attribute, $value, $parameters, $validator) {
            $data = Excel::load($value->path(), function($reader){})->get();
            $email = $firstname = $lastname = false;
            foreach ($data as $key => $value) {
                if(isset($value->roepnaam)){$firstname = true;}
                if(isset($value->achternaam)){$lastname = true;}
                if(isset($value->e_mailadres)){$email = true;};
            }
            if($email && $firstname && $lastname){
                return true;
            }else{
                return false;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}