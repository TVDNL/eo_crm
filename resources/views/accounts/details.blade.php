@extends('layouts.master')
@section('title', trans('messages.profile'))
@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="title-block">
            <h3 class="title">
                {{trans('messages.profile')}} <span class="sparkline bar" data-type="bar"></span>
                @if(Laratrust::hasRole(['docent', 'leerling']))
                    <a href="{{route('account_form', ['manual', $user])}}" class="btn btn-sm btn-primary">{{trans('messages.edit_profile')}}</a>
                @endif
            </h3>
        </div>
        <p><u>{{trans('messages.standard_info')}}</u></p>
        <div class="form-group row">
            <label for="FullName" class="col-xs-2 col-form-label">{{trans('messages.name')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="{{$user->getFullName()}}" id="FullName" readonly>
            </div>
            <label for="school_id" class="col-xs-1 col-form-label">ID:</label>
            <div class="col-xs-2">
                <input class="form-control" type="text" value="{{$user->school_id}}" id="school_id" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="dateofbirth" class="col-xs-2 col-form-label">{{trans('messages.date_of_birth')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="@if(!empty($user->dateofbirth)){{$user->dateofbirth->format('d/m/Y')}}@endif" id="dateofbirth" readonly>
            </div>
            <label for="age" class="col-xs-1 col-form-label">{{trans('messages.age')}}:</label>
            <div class="col-xs-1">
                <input class="form-control" type="text" value="{{(!empty($user->dateofbirth))?$user->dateofbirth->age:'N/A'}}" id="age" readonly>
            </div>
        </div>
        @if(!Laratrust::hasRole(['opdrachtgever']))
            <div class="form-group row">
                <label for="address" class="col-xs-2 col-form-label">{{trans('messages.address')}}:</label>
                <div class="col-xs-3">
                    <input class="form-control" type="text" value="{{$user->address}}" id="address" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="zipcode" class="col-xs-2 col-form-label">{{trans('messages.zipcode')}}:</label>
                <div class="col-xs-3">
                    <input class="form-control" type="text" value="{{$user->zipcode}}" id="zipcode" readonly>
                </div>
                <label for="city" class="col-xs-1 col-form-label">{{trans('messages.city')}}:</label>
                <div class="col-xs-2">
                    <input class="form-control" type="text" value="{{$user->city}}" id="city" readonly>
                </div>
            </div>
        @else
            <div class="form-group row">
                <label for="city" class="col-xs-2 col-form-label">{{trans('messages.city')}}:</label>
                <div class="col-xs-3">
                    <input class="form-control" type="text" value="{{$user->city}}" id="city" readonly>
                </div>
            </div>
        @endif
        <hr>
        <p><u>{{trans('messages.extra_info')}}</u></p>
            <div class="form-group row">
                <label for="level" class="col-xs-2 col-form-label">{{trans('messages.edu_level')}}:</label>
                <div class="col-xs-3">
                    <input class="form-control" type="text" value="{{$user->educational_level}}" id="level" readonly>
                </div>
            </div>
        <div class="form-group row">
            <label for="allergies" class="col-xs-2 col-form-label">{{trans('messages.allergies')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="{{$user->allergies}}" id="allergies" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="size" class="col-xs-2 col-form-label">{{trans('messages.size')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="{{$user->size}}" id="size" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="calamity" class="col-xs-2 col-form-label">{{trans('messages.emergency_nr')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="{{$user->calamity}}" id="calamity" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="bhv" class="col-xs-2 col-form-label">{{trans('messages.ero')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="@if($user->bhv == 1) {{trans('messages.yes')}} @else {{trans('messages.no')}} @endif" id="bhv" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label for="hygiene" class="col-xs-2 col-form-label">{{trans('messages.hygiene')}}:</label>
            <div class="col-xs-3">
                <input class="form-control" type="text" value="@if($user->hygiene == 1) {{trans('messages.yes')}} @else {{trans('messages.no')}} @endif" id="hygiene" readonly>
            </div>
        </div>
        <hr>
        @if(Laratrust::hasRole(['docent']))
            <p><u>{{trans('messages.notes')}}</u></p>
            <p>{{trans('messages.notes_explained')}}</p>
            <div class="form-group row">
                <div class="col-xs-6">
                    <textarea class="form-control" id="notes" rows="7" style="resize: none" readonly>{{$user->notes}}</textarea>
                </div>
            </div>
            <hr>
            <p><u>{{trans('messages.student_event_overview')}}</u></p>
            <div class="form-group row margin-bottom">
                <label class="col-sm-3 form-control-label text-xs-left">
                    {{trans('messages.total_events')}}:
                </label>
                <div class="col-sm-9">
                    {{trans('messages.public')}}: {{'&nbsp;'.count($user->events()->where('type', 'public')->get()).'&nbsp;&nbsp;/&nbsp;&nbsp;'.trans('messages.personal').': &nbsp;'.count($user->events()->where('type', 'personal')->get())}}
                </div>
            </div>
            <div class="form-group row margin-bottom">
                <label class="col-sm-3 form-control-label text-xs-left">
                    {{trans('messages.total_time')}}:
                </label>
                <div class="col-sm-9">
                    {{Helper::totalCredits($user)}}
                </div>
            </div>
            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('messages.title')}}</th>
                        <th>Status</th>
                        <th>{{trans('messages.tasks')}}</th>
                        <th>{{trans('messages.grade')}}</th>
                        <th>{{trans('messages.credited_time')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->events()->get() as $event)
                        <tr>
                            <td id="title"><a href="{{route('event_details', ['event' => $event->id])}}">{{$event->title}}</a></td>
                            <td>{{$event->participants()->find($user->id)->pivot->status}}</td>
                            <td>
                                {{
                                    count(
                                        $event->tasks()->whereHas('assignees', function($q) use ($user){
                                                $q->where('user_id', '=', $user->id);
                                        })->get()
                                    )
                                }}
                            </td>
                            <td>{{($event->participants()->find($user->id)->pivot->grade == null)? 'N/A' : $event->participants()->find($user->id)->pivot->grade}}</td>
                            <td>{{($event->participants()->find($user->id)->pivot->credited_time == null)? 'N/A' : $event->participants()->find($user->id)->pivot->credited_time}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @push('scripts')
                <script>
                    $(document).ready(function() {
                        //initialize the datatable
                        $('#table').DataTable({
                            "language": {
                                @if(App::getLocale() == 'en')
                                "url": "{{URL::asset('assets/dataTables/en.json')}}"
                                @else
                                "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                                @endif
                            },
                            columns: [
                                //title
                                null,
                                //status
                                null,
                                //tasks
                                null,
                                //grade
                                null,
                                //credited time
                                null
                            ]
                        });
                    });
                </script>
            @endpush
        @endif
    </div>
@endsection