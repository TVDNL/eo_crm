<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <!-- use the following meta to force IE use its most up to date rendering engine -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title> @yield('title') </title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300" />
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/metisMenu/2.5.2/metisMenu.min.css">
        <link href="{{URL::asset('assets/css/master.css')}}" rel="stylesheet">
        @stack('plugin-css')
        @stack('custom-css')
        <style>
            .sidebar-header .brand{font-size: 16px; padding-left: 7px}
            .input_help{cursor: pointer}
            ::-webkit-scrollbar{
                height: 11px;
            }
        </style>
    </head>
    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
                <header class="header">
                    @if (!Auth::guest())
                        <div class="header-block header-block-collapse hidden-lg-up">
                            <button class="collapse-btn" id="sidebar-collapse-btn">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <div class="header-block header-block-search hidden-sm-down">
                            <form class="typeahead" role="search">
                                <div class="input-container"> <i class="fa fa-search"></i> <input type="search" id="search-input" name="q" placeholder="{{trans('messages.search')}}" autocomplete="off">
                                    <div class="underline"></div>
                                </div>
                            </form>
                        </div>
                    @endif
                    <div class="header-block header-block-buttons">
                        {{--place here links/buttons to show in the middle on the top bar--}}
                        <img src="{{URL::asset('assets/p-trans.gif')}}" alt="Noorderpoort Logo" width="270px" height="65px">
                    </div>
                    <div class="header-block header-block-nav">
                        <ul class="nav-profile">
                            {{--<li class="notifications new">--}}
                                {{--<a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup>--}}
                                        {{--<span class="counter">8</span>--}}
                                    {{--</sup> </a>--}}
                                {{--<div class="dropdown-menu notifications-dropdown-menu">--}}
                                    {{--<ul class="notifications-container">--}}
                                        {{--Put here <li></li> with all the notifications--}}
                                    {{--</ul>--}}
                                    {{--<footer>--}}
                                        {{--<ul>--}}
                                            {{--<li> <a href="">--}}
                                                    {{--View All--}}
                                                {{--</a> </li>--}}
                                        {{--</ul>--}}
                                    {{--</footer>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    {{ Config::get('languages')[App::getLocale()] }}<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    @foreach (Config::get('languages') as $lang => $language)
                                        @if ($lang != App::getLocale())
                                            <li >
                                                <a style="padding-bottom: 10px; padding-top: 10px; margin-bottom: 5px; margin-top: 5px" class="dropdown-item" href="{{ route('lang.switch', $lang) }}"><i class="fa fa-globe icon"></i> {{$language}}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                            @if (!Auth::guest())
                                <li class="profile dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                        {{--<div class="img" style="background-image: url('https://avatars3.githubusercontent.com/u/3959008?v=3&s=40')"> </div>--}}
                                        <span class="name">
                                             {{ Auth::user()->firstname }}
                                        </span>
                                    </a>
                                    <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                                        {{--<a class="dropdown-item" href="#"> <i class="fa fa-user icon"></i> Profile </a>--}}
                                        {{--<a class="dropdown-item" href="#"> <i class="fa fa-bell icon"></i> Notifications </a>--}}
                                        {{--<a class="dropdown-item" href="#"> <i class="fa fa-gear icon"></i> Settings </a>--}}
                                        {{--<div class="dropdown-divider"></div>--}}
                                        <a class="dropdown-item" href="{{route('logout')}}"> <i class="fa fa-power-off icon"></i> Logout </a>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>
                </header>
                <aside class="sidebar">
                    <div class="sidebar-container">
                        <div class="sidebar-header">
                            <div class="brand">
                                <a href="{{route('home')}}"><div class="logo"> <span class="l l1"></span> <span class="l l2"></span> <span class="l l3"></span> <span class="l l4"></span> <span class="l l5"></span> </div> Eventmanager </div></a>
                                {{--<a href="{{route('home')}}"><div class="logo"><img src="https://pbs.twimg.com/profile_images/616188327499186178/J3WZov6D.jpg" width="150px" alt="Logo"></div> Eventmanager</a>--}}
                                {{--<a href="{{route('home')}}">Noorderpoort Eventmanager</a>--}}
                        </div>
                        @if (!Auth::guest())
                            <nav class="menu">
                                <ul class="nav metismenu" id="sidebar-menu">
                                    @if(!Laratrust::hasRole(['opdrachtgever']))
                                        <li id="home">
                                            <a href="{{route('home')}}"> <i class="fa fa-home"></i> Home </a>
                                        </li>
                                    @endif
                                    @if(Laratrust::hasRole(['leerling']))
                                        <li id="profile">
                                            <a href="{{route('profile', [Auth::id()])}}"> <i class="fa fa-user"></i> {{trans('messages.profile')}} </a>
                                        </li>
                                    @endif
                                    @if(Laratrust::hasRole(['leerling', 'opdrachtgever']))
                                        <li id="event">
                                            <a href="{{route('event_overview')}}"> <i class="fa fa-university"></i> {{trans('messages.events')}} </a>
                                        </li>
                                    @endif
                                    @if(Laratrust::hasRole(['admin', 'docent']))
                                        <li id="event">
                                            <a href=""> <i class="fa fa-th-large"></i> {{trans('messages.events_manager')}} <i class="fa arrow"></i> </a>
                                            <ul>
                                                <li>
                                                    <a href="{{route('event_overview')}}">
                                                        {{trans('messages.overview')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('event_create')}}">
                                                        <i class="fa fa-plus"></i> {{trans('messages.create_new_event')}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Laratrust::hasRole(['admin', 'docent']))
                                        <li id="account">
                                            <a href=""> <i class="fa fa-th-large"></i> {{trans('messages.acc_manager')}}<i class="fa arrow"></i> </a>
                                            <ul>
                                                <li>
                                                    <a href="{{route('account_overview')}}">
                                                        {{trans('messages.overview')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('account_form', ['manual'])}}">
                                                        <i class="fa fa-plus"></i> {{trans('messages.new_acc')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('account_form', ['automatic'])}}">
                                                        <i class="fa fa-plus"></i> Upload excel
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Laratrust::hasRole(['admin', 'docent']))
                                        <li id="group">
                                            <a href=""> <i class="fa fa-th-large"></i> {{trans('messages.groups_manager')}} <i class="fa arrow"></i> </a>
                                            <ul>
                                                <li>
                                                    <a href="{{route('group_overview')}}">
                                                        {{trans('messages.overview')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('group_create')}}">
                                                        <i class="fa fa-plus"></i> {{trans('messages.new_group')}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        @endif
                    </div>
                    <footer class="sidebar-footer">
                       {{--Sidebar footer--}}
                    </footer>
                </aside>
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                <article class="content dashboard-page">
                    <section class="section">
                        <div class="row sameheight-container">
                            @yield('content')
                        </div>
                    </section>
                </article>
            </div>
        </div>
        <script src="{{URL::asset('assets/js/master.js')}}"></script>
        <script>
            $(document).ready(function($) {
                var users = new Bloodhound({
                    remote: {
                        url: '/find?c=users&q=%QUERY%',
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                var groups = new Bloodhound({
                    remote: {
                        url: '/find?c=groups&q=%QUERY%',
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                var events = new Bloodhound({
                    remote: {
                        url: '/find?c=events&q=%QUERY%',
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                var tasks = new Bloodhound({
                    remote: {
                        url: '/find?c=tasks&q=%QUERY%',
                        wildcard: '%QUERY%'
                    },
                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });

                $("#search-input").typeahead(
                    {
                        hint: true,
                        highlight: false,
                        minLength: 1
                    },
                    {
                        source: events.ttAdapter(),
                        displayKey: 'This string is here to prevent the results json to be put in the searchbar',
                        templates: {
                            header: [
                                '<div class="list-group search-results-dropdown">'
                            ],
                            suggestion: function (data) {
                                return '<a href="/event/details/'+data.id+'"><div class="list-group-item"><span>' + data.title + '</span><span class="pull-right" style="color: darkgray">&nbsp;&nbsp;&nbsp; Event</span></div></a>';
                            }
                        }
                    },
                    {
                        source: users.ttAdapter(),
                        displayKey: 'This string is here to prevent the results json to be put in the searchbar',
                        templates: {
                            header: [
                                '<div class="list-group search-results-dropdown">'
                            ],
                            suggestion: function (data) {
                                return '<a href="/profile/' + data.id + '"><div class="list-group-item"><span>' + data.firstname + ' ' + data.lastname_prefix + ' ' + data.lastname + '</span><span class="pull-right" style="color: darkgray">&nbsp;&nbsp;&nbsp; ' + data.roles[0].display_name + '</span></div></a>';
                            }
                        }
                    },
                    {
                        source: groups.ttAdapter(),
                        displayKey: 'This string is here to prevent the results json to be put in the searchbar',
                        templates: {
                            header: [
                                '<div class="list-group search-results-dropdown">'
                            ],
                            suggestion: function (data) {
                                return '<a href="/group/details/'+data.id+'"><div class="list-group-item"><span>' + data.name + '</span><span class="pull-right" style="color: darkgray">&nbsp;&nbsp;&nbsp; Group</span></div></a>';
                            }
                        }
                    },
                    {
                        source: tasks.ttAdapter(),
                        displayKey: 'This string is here to prevent the results json to be put in the searchbar',
                        templates: {
                        header: [
                            '<div class="list-group search-results-dropdown">'
                        ],
                            suggestion: function (data) {
                            return '<a href="/event/details/'+data.event_id+'"><div class="list-group-item"><span>' + data.title + '</span><span class="pull-right" style="color: darkgray">&nbsp;&nbsp;&nbsp; Task</span></div></a>';
                        }
                    }
                }
                );

                //Menu highlighting
                var pathname = window.location.pathname;
                var id = '';
                if(pathname.startsWith("/profile")) id = 'profile';
                if(pathname.startsWith("/account")) id = 'account';
                if(pathname.startsWith("/event")) id = 'event';
                if(pathname.startsWith("/group")) id = 'group';
                if(id == '') id = 'home';
                $('#'+id).addClass('active'); //adds hightlight color
                $('#'+id+' a').trigger('click');//expands menu
            });
        </script>
        @stack('plugin-js')
        @stack('scripts')
    </body>
</html>