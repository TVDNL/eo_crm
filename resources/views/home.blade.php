@extends('layouts.master')
@section('title', 'Home Page')
@section('content')
    @push('custom-css')
        <style>
            #title a {text-decoration: none; color: #4f5f6f}
            .margin-bottom {margin-bottom: 25px}
        </style>
    @endpush
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(Laratrust::hasRole(['leerling']))
                            <div class="title-block">
                                <h5 >
                                    {{trans('messages.my_overview')}} <span class="sparkline bar" data-type="bar"></span>
                                </h5>
                            </div>
                            <div class="form-group row margin-bottom">
                                <label class="col-sm-3 form-control-label text-xs-right">
                                    {{trans('messages.total_events')}}:
                                </label>
                                <div class="col-sm-9">
                                    {{trans('messages.public')}}: {{'&nbsp;'.count(Auth::user()->events()->where('type', 'public')->get()).' uur&nbsp;&nbsp;/&nbsp;&nbsp;'.trans('messages.personal').': &nbsp;'.count(Auth::user()->events()->where('type', 'personal')->get()).' uur'}}
                                </div>
                            </div>
                            <div class="form-group row margin-bottom">
                                <label class="col-sm-3 form-control-label text-xs-right">
                                    {{trans('messages.total')}} {{strtolower(trans('messages.credited_time'))}}:
                                </label>
                                <div class="col-sm-9">
                                    {{Helper::totalCredits(Auth::user())}}
                                </div>
                            </div>
                            <div class="title-block">
                                <h5 >
                                    {{trans('messages.my_events')}} <span class="sparkline bar" data-type="bar"></span>
                                </h5>
                            </div>
                            <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>{{trans('messages.title')}}</th>
                                        <th>Status</th>
                                        <th>{{trans('messages.tasks')}}</th>
                                        <th>{{trans('messages.grade')}}</th>
                                        <th>{{trans('messages.credited_time')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach(Auth::user()->events()->get() as $event)
                                    <tr>
                                        <td id="title"><a href="{{route('event_details', ['event' => $event->id])}}">{{$event->title}}</a></td>
                                        <td>{{$event->participants()->find(Auth::id())->pivot->status}}</td>
                                        <td>
                                            {{
                                                count(
                                                    $event->tasks()->whereHas('assignees', function($q){
                                                            $q->where('user_id', '=', Auth::id());
                                                    })->get()
                                                )
                                            }}
                                        </td>
                                        <td>{{($event->participants()->find(Auth::id())->pivot->grade == null)? 'N/A' : $event->participants()->find(Auth::id())->pivot->grade}}</td>
                                        <td>{{($event->participants()->find(Auth::id())->pivot->credited_time == null)? 'N/A' : $event->participants()->find(Auth::id())->pivot->credited_time}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        //initialize the datatable
                                        $('#table').DataTable({
                                            "language": {
                                                @if(App::getLocale() == 'en')
                                                "url": "{{URL::asset('assets/dataTables/en.json')}}"
                                                @else
                                                "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                                                @endif
                                            },
                                            columns: [
                                                //title
                                                null,
                                                //status
                                                null,
                                                //tasks
                                                null,
                                                //grade
                                                null,
                                                //credited time
                                                null
                                            ]
                                        });
                                    });
                                </script>
                            @endpush
                        @elseif(Laratrust::hasRole(['docent']))
                            <div class="title-block">
                                <h5>
                                    <b>{{trans('messages.my_events')}}</b><span class="sparkline bar" data-type="bar"></span>
                                </h5>
                            </div>
                            <table id="eventTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>{{trans('messages.title')}}</th>
                                    <th>Start</th>
                                    <th>{{trans('messages.end')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Auth::user()->events()->get() as $event)
                                    <tr>
                                        <td id="title">
                                            <a href="{{route('event_details', ['event' => $event->id])}}">{{$event->title}}</a>
                                        </td>
                                        <td>{{$event->startDate}}</td>
                                        <td>{{$event->endDate}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        //initialize the datatable
                                        $('#eventTable').DataTable({
                                            "language": {
                                                @if(App::getLocale() == 'en')
                                                "url": "{{URL::asset('assets/dataTables/en.json')}}"
                                                @else
                                                "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                                                @endif
                                            },
                                            columns: [
                                                //title
                                                null,
                                                //start
                                                {
                                                    render: function(data){
                                                        var date = moment(data);
                                                        return date.format('DD/MM/YYYY kk:ss');                                                    }
                                                },
                                                //end
                                                {
                                                    render: function(data){
                                                        var date = moment(data);
                                                        return date.format('DD/MM/YYYY kk:ss');                                                    }
                                                }
                                            ]
                                        });
                                    });
                                </script>
                            @endpush
                            <br>
                            <div class="title-block">
                                <h5 >
                                    <b>{{trans('messages.outstanding_reviews')}}</b><span class="sparkline bar" data-type="bar"></span>
                                </h5>
                            </div>
                            <table id="reviewTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>{{trans('messages.event')}}</th>
                                    <th>{{trans('messages.credited_time')}}</th>
                                    <th>{{trans('messages.grade')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($outstanding_reviews as $review)
                                    <tr>
                                        <td><a href="{{route('profile', [$review['user']->id])}}" style="text-decoration: none; color: #4f5f6f">{{$review['user']->getFullName()}}</a></td>
                                        <td id="title"><a href="{{route('event_details', ['event' => $review['event']->id])}}">{{$review['event']->title}}</a></td>
                                        <td>{!!($review['user']->pivot->credited_time == 0)? '<i class="fa fa-times" aria-hidden="true"></i>' : $review['user']->pivot->credited_time !!}</td>
                                        <td>{!!($review['user']->pivot->grade == null)? '<i class="fa fa-times" aria-hidden="true"></i>' : $review['user']->pivot->grade !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @push('scripts')
                                <script>
                                    $(document).ready(function() {
                                        //initialize the datatable
                                        $('#reviewTable').DataTable({
                                            "language": {
                                                @if(App::getLocale() == 'en')
                                                    "url": "{{URL::asset('assets/dataTables/en.json')}}"
                                                @else
                                                    "url": "{{URL::asset('assets/dataTables/nl.json')}}"
                                                @endif
                                            },
                                            columns: [
                                                //student
                                                null,
                                                //event title
                                                null,
                                                //crdits
                                                null,
                                                //grade
                                                null
                                            ]
                                        });
                                    });
                                </script>
                            @endpush
                        @elseif(Laratrust::hasRole(['opdrachtgever']))
                            Welkom op Event Manager

                            Hier kunt u ten alle tijden de gegevens van u event bekijken, de status van het aantal opgegeven studenten checken en bijvoorbeeld eventuele roosters uitdraaien.

                            Links in de menubalk vind u onder de knop "Events" de events waar u beheerder van bent. Wanneer u in een gekozen event gaat kunt u daar dmv het potloodje het event aanpassen waar nodig.

                            Zodra er studenten zijn toegevoegd aan uw event kunnen er bijv. roosters worden geëxporteerd.
                        @else
                            Welkom, U bent nu ingelogd.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection